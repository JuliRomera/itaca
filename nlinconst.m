function [c, ceq] = nlinconst(x,concEntradaMax, concEntradaMin, reduccions,Cin, supTractaments, supPlanta, Cout, optim)

%nlinconst:  Funci� de restriccions per al c�lcul d'optimitzaci� de la
%planta de tractament.
%
% Variables d'entrada:
%   x:           Variables doptimitzaci�. Gestionades internament pels
%                algoritmes gen�tics.
%   concEntrada: Concentracions admisibles m�ximes d'entrada per a cada tractament i aigua.
%                Par�metre de disseny dels tractaments.
%   reduccions:  Capacitat de reducci� per cada contaminant de cada tractament i aigua.
%                Par�metres de disseny dels tractaments.
%   Cin:         Concentracions d'entrada per a cada tipus d'aigua.
%                Par�metres d'entrada de la planta.
%   Cout:        Concentracions de sortida Desitjades.
%                Par�metres de disseny de la planta.
%
% Variables de sortida:
%   c:           Vector de restriccions d'igualtat no linials.
%                Gestionat internament pels algoritmes gen�tics.
%   ceq:         Vector de restriccions d'inegualtat no linials.
%                Gestionat internament pels algoritmes gen�tics.


c   = [];
ceq = [];


    %% ---- Restriccions ---- %%
    x = floor(x);

    dif = unique(x); %Processos diferents
    c   = 1000*(length(x) - length(dif));

    % Restriccio del tractament d'acord amb la seva concentraci� m�xima
    % d'entrada admisible
    for i=1:length(Cin)
        aux = Cin(i)*[1 cumprod(reduccions(i,x))];
        c   = [c aux(end)-Cout(i)];
        c   = [c (aux(1:end-1)-concEntradaMax(i,x))];
        c   = [c (concEntradaMin(i,x)-aux(1:end-1))];
    end
    % Restricci� segons la superf�cie disponible
    c = [c (sum(supTractaments(x))-supPlanta)];
end
