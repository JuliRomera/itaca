function cost = gaCost(x, costosMin, costosMax, pesos, reduccions, cin, cout,optim)

%gaCost:  Funci� de cost. Aqu� es tenen en compte els diferents termes a
%optimitzar.
%
% Variables d'entrada:
%   x:           Variables doptimitzaci�. Gestionades internament pels
%                algoritmes gen�tics.
%   criteris:    Termes de la funci� de cost. Cost econ�mic, Impacte ambiental, etc.
%
% Variables de sortida:
%   cost:        Cost de la funci� objectiu total.

    aux  = [];
    a    = 0;
    cost = 0;

    %Agafo el cost nom�s dels processos necessaris per cumplir la restricci� de
    %concentraci� de sortida
    x = floor(x);

    for i=1:length(cin)
        aux = cin(i)*cumprod(reduccions(i,x));
        if a < length(find(aux>=cout(i))), a = length(find(aux>=cout(i))); end;
    end

    a = min(length(x),a+1);
    %I aqu� calculo el cost
    if (~isempty(costosMin) && ~isempty(costosMax))
        if a>1
            auxCost1 = sum(costosMin(x(:,1:a),:));
        else
            auxCost1 = costosMin(x(:,1:a),:);
        end

        if a>1
            auxCost2 = sum(1-costosMax(x(:,1:a),:));
        else
            auxCost2 = 1-costosMax(x(:,1:a),:);
        end
        auxCost = [auxCost1 auxCost2];
    elseif (~isempty(costosMin))
        if a>1
            auxCost = sum(costosMin(x(:,1:a),:));
        else
            auxCost = costosMin(x(:,1:a),:);
        end
    else
        if a>1
            auxCost = sum(1-costosMax(x(:,1:a),:));
        else
            auxCost = 1-costosMax(x(:,1:a),:);
        end
    end
    cost = sum(auxCost.*pesos);
end
