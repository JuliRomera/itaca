function [solucions,cost] = gaMain(cin, cout, concEntradaMax, concEntradaMin, reduccions, costosMin, costosMax, pesos, etapes, niter, supTractaments, supPlanta, generacions, optim)

%gaMain:  Funci� Principal dels Algoritmes Gen�tics. Aqu� �s on es crida la
%funci� d'optimitzaci� el nombre d'iteracions definides.
%
% Variables d'entrada:
%   cin:         Concentracions d'entrada per a cada tipus d'aigua.
%                Par�metres d'entrada de la planta.
%   cout:        Concentracions de sortida Desitjades.
%                Par�metres de disseny de la planta.
%   concEntrada: Concentracions admisibles m�ximes d'entrada per a cada tractament i aigua.
%                Par�metre de disseny dels tractaments.
%   reduccions:  Capacitat de reducci� per cada contaminant de cada tractament i aigua.
%                Par�metres de disseny dels tractaments.
%   costos:      vector amb el cost econ�mic de cada tractament.
%                Par�metres de disseny dels tractaments.
%   etapes:      Nombre d'etapes m�xima per a la planta de tractament.
%                Par�metres de disseny de la planta.
%   niter:       Nombre d'iteracions per la optimitzaci�. Es faran tantes
%                optimitzacions com nombre d'iteracions i s'agafa la millor com a
%                resultat final.
%
% Variables de sortida:
%   solucions:   Matriu amb el resultat de totes les optimitzacions de dimensions (etapes,niter)
%   cost:        Vector amb el cost econ�mic de cada optimitzaci�.

x          = zeros(1,etapes);
cost       = [];
solucions  = [];
iteracions = niter;             %Maximum number of iterations of the GA
fi         = false;             %End of GA iterations variable
j          = 1;                 %Iteration counter
costAnt    = 1e50;

IntCon = 1:etapes;

h = waitbar(0,'Proceso... 0%','Name','Optimizaci�n en curso');

while fi==false && j < iteracions
    
    switch(optim)
        case 'pattern'

            options = psoptimset('CompletePoll','on','CompleteSearch','on',...
                'MeshContraction',0.5,'TimeLimit',120,...
                'InitialMeshSize',size(concEntradaMax,2),'ScaleMesh','on',...
                'MaxMeshSize',size(concEntradaMax,2),...
                'UseParallel','always','Vectorized','off',...
                'SearchMethod',{@searchlhs,generacions,3*generacions},...
                'PollingOrder','Success','Display','off');
            
            [x,cost(j),exitflag,output] = patternsearch(@(x)gaCost(x, costosMin', costosMax', pesos, reduccions, cin, cout,optim),...
                zeros(1,etapes),[],[],[],[],...
                ones(1,etapes),...                        %lo
                size(concEntradaMax,2)*ones(1,etapes),... %up
                @(x)nlinconst(x,concEntradaMax, concEntradaMin,reduccions,cin, supTractaments, supPlanta, cout,optim),...
                options);%#ok<NASGU,ASGLU,AGROW>
            
            if exitflag <0
                disp(['exitflag: ' num2str(exitflag) ', maxConstraint: ' num2str(output.maxconstraint)]);
                disp(['message: ' num2str(output.message) ', Function evaluation: ' num2str(output.funccount)]);
                disp(['Output: ' num2str(x)]);
                disp(['Valor restriccions: ' num2str(nlinconst(x,concEntradaMax, concEntradaMin,reduccions,cin, supTractaments, supPlanta, cout, optim))]);
                disp('Output:');
                disp(' ');
                disp(output);
                disp(' ------------------ ');
            end

        otherwise
            
            options = gaoptimset('Generations',generacions,'Display','off',...
                'TolFun',1e-5,'MigrationInterval',20,'UseParallel','always',...
                'stallTimeLimit',120,'TimeLimit',120,'StallGenLimit',1000);
            
            [x,cost(j),exitflag,output,population,score] = ga(@(x)gaCost(x, costosMin', costosMax', pesos, reduccions, cin, cout,optim),...
                etapes,[],[],[],[],...
                ones(1,etapes),...                    %lo
                size(concEntradaMax,2)*ones(1,etapes),...  %up
                @(x)nlinconst(x,concEntradaMax, concEntradaMin,reduccions,cin, supTractaments, supPlanta, cout, optim),...
                IntCon,options);%#ok<NASGU,ASGLU,AGROW>
            
            if exitflag <0
                disp(['exitflag: ' num2str(exitflag) ', maxConstraint: ' num2str(output.maxconstraint)]);
                disp(['generations: ' num2str(output.generations) ', Function evaluation: ' num2str(output.funccount)]);
                disp(find(nlinconst(x,concEntradaMax, concEntradaMin,reduccions,cin, supTractaments, supPlanta, cout,optim)>0));
                disp(' ');
            end
    end
        
    [costEconomic] = gaCost(x, costosMin', costosMax', pesos, reduccions, cin, cout,optim);
    if ((exitflag>=0) && costEconomic <= costAnt)
        solucions = [solucions;floor(x)]; %#ok<AGROW>
        costAnt   = costEconomic;
    end
    j = j+1;

    if exist('h')
        try
            waitbar(j/iteracions,h,sprintf('Proceso... %0.0f%%',j*100/iteracions));
        catch lasterror
            clear h;
        end
    end

    disp(['Partial cost: ', num2str(costEconomic) '(' num2str(costAnt) ')' '    Iteration number ', num2str(j-1), '/', num2str(iteracions-1)]);
    disp(floor(x));
end
disp(['Acabat a la iteracio: ' num2str(j-1)]);
if exist('h')
    try
        close(h);
    catch lasterror
        clear h;
    end
end


