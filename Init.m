function [solucio,noAssolits] = Init(cin, cout, concEntradaMax, concEntradaMin, reduccions, costosMin, costosMax, pesos, nIteracions,supTractaments, supPlanta, generacions, numEtapes, optim)

    %Init:  Funci� inical per a la optimitzacio amb Algoritmes Gen�tics. Aqu� �s on es crida la
    %funci� principal amb les dades del problema. Acabada la optimitzaci� es
    %mostren els resultats a la l�nia de comandes del Matlab.
    %
    % Variables d'entrada:
    %   cin:         Concentracions d'entrada per a cada tipus d'aigua.
    %                Par�metres d'entrada de la planta.
    %   cout:        Concentracions de sortida Desitjades.
    %                Par�metres de disseny de la planta.
    %   concEntradaMax: Concentracions admisibles m�ximes d'entrada per a cada tractament i aigua.
    %                Par�metre de disseny dels tractaments.
    %   concEntradaMin: Concentracions admisibles m�nimes d'entrada per a cada tractament i aigua.
    %                Par�metre de disseny dels tractaments.
    %   reduccions:  Capacitat de reducci� per cada contaminant de cada tractament i aigua.
    %                Par�metres de disseny dels tractaments.
    %   costosMin:   Cost total dels tractamens a minimitzar per la funcio de cost.
    %   costosMax:   Cost total dels tractamens a maximitzar per la funcio de cost.
    %   costos:      vector amb el cost econ�mic de cada tractament.
    %                Par�metres de disseny dels tractaments.
    %   pesos:       Pesos dels termes de la funcion de cost.
    %   nIteracions  N�mero d'iteracions que far� l'optimitzador.
    %   supTractaments:Superaf�cie requerida per cada tactament.
    %   supPlanta:   Superf�cie de la planta (restricci�)
    %   generacions: Par�metre de l'optimitzador pel cas dels Gen�tics
    %   optim:       Tipus d'optimitzador triat per l'usuari
    %
    % Variables de sortida:
    %   solucions:   Matriu amb el resultat de totes les optimitzacions de dimensions (etapes,niter)

    %SCRIPT OptimTreatmentDesigner.m

    % ConcEntrada:
    % Columnes: Concentraci� m�xima admisible d'entrada per a cada proc�s
    % files:    Contaminant

    % Reduccions;
    % Columnes: Reducci� del contameninant de la fila x
    % files:    Contaminant

    %% Data input
    %nIteracions = 10;
    if ~isempty(costosMin)
        Etapes      = min(numEtapes,size(costosMin,2));
    else
        Etapes      = min(numEtapes,size(costosMax,2));
    end

    %Etapes = 5;

    %% C�lcul Soluci�
    [solucions,cost] = gaMain(cin, cout, concEntradaMax, concEntradaMin,...
        reduccions, costosMin, costosMax, pesos, Etapes, nIteracions+1,...
        supTractaments, supPlanta, generacions, optim);

    %% Display results
    solucio    = [];
    noAssolits = [];
   % try
        disp('Solucions:');
        disp(solucions);
        disp('----------------------------------');
        disp('** REDUCCI� PER CADA TRACTAMENT **');
        disp(' ');
        disp('===============================');
        disp('tractaments seleccionats     : millor soluci� trobada.');
        disp('Reduccions(i)                : reduccions de cada tractament de la soluci� per al par�metre "i".');
        disp('concentraci� desitjada (Cout): concentraci� de l''aigua de sortida per al par�metre "i".');
        disp('seq��ncia concentracions     : concentracions resultants de l''aplicaci� de la reducci� de cada tractament.');
        disp('costos tractaments           : costos acumulats per ala tractaments sel�leccionats.');
        disp('===============================');

        [solucio, noAssolits] = calculaSolucio(cin, cout, solucions, reduccions, Etapes, costosMin,costosMax, 2);

end

%%Calcul de la soluci� desitjada
function [solucio,noAssolits] = calculaSolucio(cin, cout, solucions, reduccions, Etapes, costosMin, costosMax, numSolucio)

    solucio       = [];
    noAssolits    = [];
    numSolucio    = 0;
    solucioTotals = 0;
    costos = [costosMin;costosMax];
    while solucioTotals<3

        if size(solucions,1)<=numSolucio
            %solucio    = [];
            %noAssolits = [];
            return;
        end

        solucioInt    = [];
        noAssolitsInt = [];
        for ii=1:length(cin)
            %Miro per cada un dels par�metres de l'aigua quants tractaments
            %necessito per arribar a la seva concetraci� de sortida.
            %Agafar� el pitjor cas.
            aux = cin(ii)*[1 cumprod(reduccions(ii,solucions(end-numSolucio,:)))];

            processos = length(find(aux(2:end) >= cout(ii)));
            if processos<Etapes
                aux2 = solucions(end-numSolucio,1:length(find(aux(2:end) >= cout(ii)))+1);
            else
                aux2 = solucions(end-numSolucio,1:length(find(aux(2:end) >= cout(ii))));
            end
            if length(aux2)>length(solucioInt)
                solucioInt = aux2;
            end
            if aux(end)>cout(ii)
                noAssolitsInt = [noAssolitsInt ii];
            end      
        end
        trobat = 0;
        if ~isempty(solucio)
            %Miro si el resulta trobat a solucioInt ja estava resgistrat a
            %solucio
            for j=1:size(solucio,1)
                [f,c] = find(solucio(end-j+1,:));
                if length(solucioInt)==length(solucio(end-j+1,c))
                    if isempty(find(solucioInt-solucio(end-j+1,c)))
                        trobat = 1;
                        break;
                    end
                end
            end
            if ~trobat
                %En cas que no estigui registrat (trobat) el registro
                if length(solucioInt) ~= size(solucio,2)
                    if size(solucio,2) > length(solucioInt)
                        solucio    = [[solucioInt zeros(1,size(solucio,2) - length(solucioInt))];solucio];
                    else
                        %solucio    = [solucioInt;[solucio zeros(1,size(solucio,2) - length(solucioInt))]];
                        solucio    = [solucioInt;[solucio zeros(size(solucio,1),length(solucioInt)-size(solucio,2))]];
                    end
                    noAssolits    = noAssolitsInt;
                    solucioTotals = solucioTotals + 1;
                else
                    solucio       = [solucioInt;solucio];
                    noAssolits    = noAssolitsInt;
                    solucioTotals = solucioTotals + 1;
                end
            end
        else
            %La primera vegada encara no tenim resultats registrats
            solucio       = solucioInt;
            noAssolits    = noAssolitsInt;
            solucioTotals = solucioTotals + 1;
        end
        numSolucio = numSolucio +1;
        
        disp('---------------');
        if ~isempty(noAssolitsInt)
            disp(['Els seg�ents par�metres no s''han pogut redu�r: ' num2str(noAssolitsInt)]);
        end
        disp(['COSTOS: ' num2str(cumsum(sum(costos(:,solucions(end,1:length(solucioInt))))))]);
    end
%     solucio    = solucioInt;
%     noAssolits = noAssolitsInt;
end
