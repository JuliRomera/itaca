function [out] = CalculoPlantaTratamiento_v8
   

    clc
    clear

    global hlp;
    
    global criteri;
    global indicadors;
    global indList;
    global contaminants; %Llista de noms dels contaminants
    global paramsPlanta; %Par�metres pel dimensionament de la planta(cabals, superficie i tmps impl)
    global textAgreg;
    global Cin;
    global Cout;
    global numCin;
    global numCinSeleccio;
    global numCout;
    global numCoutSeleccio;
    global aiguaEntrada;
    global aiguaSortida;
    global componentsAOptimitzar;

    global solucio;

    %CRITERIS / INDICADORS
    global c;
    global ind;
    %global criterisCriteris;
    global indListCriteris;
    global indicadorsCriteris;    
    global indListCriterisSeleccio;
    global CriterisPopup;
    global addButton;
    global textInd;
    global textSelected;
    
    %ENTRADA / SORTIDA
    global ea;
    global aiguaE;
    global aiguaS;
    global descripEntradaTxt;
    global descripSortidaTxt;
    
    aiguaE = 0;
    aiguaS = 0;
  
    % PLANTA
    global p;
    global cabalMin;
    global cabalMax;
    global superficie;
    global tempsImplantacio;
    global cabalMinPlanta;
    global cabalMaxPlanta;
    global superficiePlanta;
    global tempsImplantacioPlanta;
    global numEtapesPlanta;
    
    cabalMin         = 0;
    cabalMax         = 0;
    superficie       = 0;
    tempsImplantacio = 0;
    numEtapes        = 5;
    
    %OPTIMITZACIO
    global o;
    global optimitzador;
    global iteracions;
    global iteracionsOptimitzador;
    global generacions;
    global iterationsLimit;
    global generacionsOptimitzador;
    global optimValor;
    global iteracionsValor;
    global iteracionsEdit;
    global generacionsValor;
    global generacionsEdit;
    
    optimitzador    = 'genetics';
    iteracions      = 10;
    generacions     = 10000;
    iteracionsLimit = 500;

    %RESULTATS
    global textResultat;
    global costImplantacioValor;
    global costOperacioValor;
    global impacteAmbientalValor;
    global valoritzacioValor;
    
    numData      = [];
    txtData      = [];
    rawData      = [];
    criteris     = [];
    criteri      = 0;
    criteriVisualitzacio = 1;
    indicadors   = [];
    indList      = cell(1,2);
    contaminants = cell(1,1);
    paramsPlanta = cell(1,1); 
    Cin          = cell(1,1);
    Cout         = cell(1,1);
    aiguaEntrada = 0;
    aiguaSortida = 0;
    componentsAOptimitzar = [];
    solucio      = [];
    
    set(0,'Units','normalized');
    
    f = figure('NumberTitle','off','Visible','off',...%'Position',[360,400,1265,480],...%480,650,485],...
        'menubar','none','Toolbar','none','Resize','off');

    processImatge = imread('fondo.jpg');
    Y = imresize(processImatge,5);
    imshow(Y,'Border','tight');%,'InitialMagnification','fit');

    pfig   = get(f,'position');
    colfig = [0,73/255,145/255];
    coefX  = pfig(3)/1054;
    coefY  = pfig(4)/498;
    
    posPanDatos = [.013 .49 .13 .295];
    pos         = pfig.*posPanDatos;
    posTitol    = [10 pos(4)*0.9257 pos(3)*0.7 pos(4)*0.1157];
    
    panDatos    = uipanel(...
        'TitlePosition','lefttop','Position',posPanDatos,'BackgroundColor',colfig);
    panDatosTitol  = uicontrol('parent',panDatos,'Style','text','String','Carga de datos','FontSize',max(5,posTitol(4)*0.58),...%10*coefX,...
            'Position',posTitol,'BackgroundColor','white',... %[10*coefX,136*coefY,100*coefX,17*coefY]
            'HorizontalAlignment','Center');
        
    posPanconfiguracion   = [.15 .49 .535 .295];
    pos                   = pfig.*posPanconfiguracion;
    posConfiguracionTitol = [10 pos(4)*0.9257 pos(3)*0.3 pos(4)*0.1157];

    panconfiguracion = uipanel(...
        'TitlePosition','lefttop','Position',posPanconfiguracion,'BackgroundColor',colfig);
    panConfiguracionTitol  = uicontrol('parent',panconfiguracion,'Style','text','String','Configuraci�n','FontSize',max(7,posConfiguracionTitol(4)*0.58),...%10*coefX,...
            'Position',posConfiguracionTitol,'BackgroundColor','white',... %[10*coefX,136*coefY,90*coefX,17*coefY]
            'HorizontalAlignment','Center');
        
    posPanoptimizacion   = [.693 .49 .149 .295];
    pos                  = pfig.*posPanoptimizacion;
    posOptimizacionTitol = [10 pos(4)*0.9257 pos(3)*0.7 pos(4)*0.1157];%90 , 0.5731
    
    panoptimizacion = uipanel(...
        'TitlePosition','lefttop','Position',posPanoptimizacion,'BackgroundColor',colfig);
    panOptimizacionTitol  = uicontrol('parent',panoptimizacion,'Style','text','String','Optimizaci�n','FontSize',max(7,posOptimizacionTitol(4)*0.58),...%10*coefX,...
            'Position',posOptimizacionTitol,'BackgroundColor','white',... %[10*coefX,136*coefY,90*coefX,17*coefY]
            'HorizontalAlignment','Center');
        
    posPanresultados   = [.849 .49 .14 .295];
    pos                = pfig.*posPanresultados;
    posResultadosTitol = [10 pos(4)*0.9257 pos(3)*0.7 pos(4)*0.1157];
    
    panresultados = uipanel(...
        'TitlePosition','lefttop','Position',posPanresultados,'BackgroundColor',colfig);
    panResultadosTitol  = uicontrol('parent',panresultados,'Style','text','String','Resultados','FontSize',max(7,posResultadosTitol(4)*0.58),...%10*coefX,...
            'Position',posResultadosTitol,'BackgroundColor','white',... %[10*coefX,136*coefY,80*coefX,17*coefY]
            'HorizontalAlignment','Center');
        
    posPanparametros0  = [.013 .15 .2 .295];
    pos                = pfig.*posPanparametros0;
    posParametrosTitol = [10 pos(4)*0.95 pos(3)*0.8 pos(4)*0.1];%100  0.077
    posAiguesESTitol   = [10 pos(4)*0.7487 pos(3)*0.9 pos(4)*0.1157];%170
    posEntradaTitol    = [10 pos(4)*0.6126 pos(3)*0.9 pos(4)*0.1157];
    posSalidaTitol     = [10 pos(4)*0.2723 pos(3)*0.9 pos(4)*0.1157];
    
    panparametros0 = uipanel(...
            'TitlePosition','lefttop','Position',posPanparametros0,'BackgroundColor',colfig);
        panParametrosTitol  = uicontrol('parent',panparametros0,'Style','text','String','Par�metros Seleccionados','FontSize',max(7,posParametrosTitol(4)*0.5882),...%10*coefX,...
            'Position',posParametrosTitol,'BackgroundColor','white',...  %[10*coefX,136*coefY,170*coefX,17*coefY]
            'HorizontalAlignment','Center');
        panAiguesESTitol  = uicontrol('parent',panparametros0,'Style','text','String','Aguas de entrada y salida','FontSize',max(7,posAiguesESTitol(4)*0.5882),...%9*coefX,...
            'Position',posAiguesESTitol,'ForegroundColor','white','BackgroundColor',colfig,... %[15*coefX,110*coefY,170*coefX,17*coefY]
            'HorizontalAlignment','Left','FontWeight','Bold');
        panEntradaTitol  = uicontrol('parent',panparametros0,'Style','text','String','Entrada:','FontSize',max(7,posEntradaTitol(4)*0.5882),...%9*coefX,...
            'Position',posEntradaTitol,'ForegroundColor','white','BackgroundColor',colfig,...  %[15*coefX,90*coefY,170*coefX,17*coefY]
            'HorizontalAlignment','Left');
        panSalidaTitol  = uicontrol('parent',panparametros0,'Style','text','String','Salida:','FontSize',max(7,posSalidaTitol(4)*0.5882),...%9*coefX,...
            'Position',posSalidaTitol,'ForegroundColor','white','BackgroundColor',colfig,... %[15*coefX,40*coefY,170*coefX,17*coefY]
            'HorizontalAlignment','Left');
        
    posPanparametros1     = [.211 .15 .23 .295];
    pos                   = pfig.*posPanparametros1;
    posPlantaTitol        = [10 pos(4)*0.7487 pos(3)*0.9 pos(4)*0.1157];
    posQminTitol          = [10 pos(4)*0.5526 pos(3)*0.9 pos(4)*0.1157];
    posQmaxTitol          = [10 pos(4)*0.39 pos(3)*0.9 pos(4)*0.1157];
    posSmaxTitol          = [10 pos(4)*0.23 pos(3)*0.9 pos(4)*0.1157];
    posTimplantacionTitol = [10 pos(4)*0.075 pos(3)*0.9 pos(4)*0.1157];
    
    posQminUnits          = [pos(3)-pos(3)*0.2 pos(4)*0.5526 pos(3)*0.175 pos(4)*0.1157];
    posQmaxUnits          = [pos(3)-pos(3)*0.2 pos(4)*0.39 pos(3)*0.17 pos(4)*0.1157];
    posSmaxUnits          = [pos(3)-pos(3)*0.2 pos(4)*0.23 pos(3)*0.17 pos(4)*0.1157];
    posTimplantacionUnits = [pos(3)-pos(3)*0.2 pos(4)*0.075 pos(3)*0.17 pos(4)*0.1157];

    panparametros1 = uipanel(...
        'TitlePosition','lefttop','Position',[.211 .15 .23 .295],'BackgroundColor',colfig);
        panPlantaTitol  = uicontrol('parent',panparametros1,'Style','text','String','Planta de Tratamiento','FontSize',max(7,posPlantaTitol(4)*0.5882),...%9*coefX,...
            'Position',posPlantaTitol,'ForegroundColor','white','BackgroundColor',colfig,... %[10*coefX,110*coefY,170*coefX,17*coefY]
            'HorizontalAlignment','Left','FontWeight','Bold');
        panQminTitol  = uicontrol('parent',panparametros1,'Style','text','String','Caudal m�nimo','FontSize',max(4,posQminTitol(4)*0.53),...%9*coefX,...
            'Position',posQminTitol,'ForegroundColor','white','BackgroundColor',colfig,... %[10*coefX,80*coefY,215*coefX,17*coefY]
            'HorizontalAlignment','Left');
        panQmaxTitol  = uicontrol('parent',panparametros1,'Style','text','String','Caudal m�ximo','FontSize',max(4,posSalidaTitol(4)*0.53),...%9*coefX,...
            'Position',posQmaxTitol,'ForegroundColor','white','BackgroundColor',colfig,... %[10*coefX,57*coefY,215*coefX,17*coefY]
            'HorizontalAlignment','Left');
        panSmaxTitol  = uicontrol('parent',panparametros1,'Style','text','String','Superf�cie m�xima','FontSize',max(7,posQmaxTitol(4)*0.53),...%9*coefX,...
            'Position',posSmaxTitol,'ForegroundColor','white','BackgroundColor',colfig,... %[10*coefX,34*coefY,215*coefX,17*coefY]
            'HorizontalAlignment','Left');
        panTimplantacionTitol  = uicontrol('parent',panparametros1,'Style','text','String','Tiempo Implantaci�n','FontSize',max(7,posTimplantacionTitol(4)*0.53),...%9*coefX,...
            'Position',posTimplantacionTitol,'ForegroundColor','white','BackgroundColor',colfig,... %[10*coefX,11*coefY,215*coefX,17*coefY]
            'HorizontalAlignment','Left');
        %Unitats
        panQminUnitats  = uicontrol('parent',panparametros1,'Style','text','String','m3/h','FontSize',max(7,posQminTitol(4)*0.53),...
            'Position',posQminUnits,'ForegroundColor','white','BackgroundColor',colfig,...
            'HorizontalAlignment','Left');
        panQmaxUnitats  = uicontrol('parent',panparametros1,'Style','text','String','m3/h','FontSize',max(7,posSalidaTitol(4)*0.53),...
            'Position',posQmaxUnits,'ForegroundColor','white','BackgroundColor',colfig,...
            'HorizontalAlignment','Left');
        panSmaxUnitats  = uicontrol('parent',panparametros1,'Style','text','String','m2/UF','FontSize',max(7,posQmaxTitol(4)*0.53),...
            'Position',posSmaxUnits,'ForegroundColor','white','BackgroundColor',colfig,...
            'HorizontalAlignment','Left');
        panTimplantacionUnitats  = uicontrol('parent',panparametros1,'Style','text','String','sem.','FontSize',max(7,posTimplantacionTitol(4)*0.53),...
            'Position',posTimplantacionUnits,'ForegroundColor','white','BackgroundColor',colfig,...
            'HorizontalAlignment','Left');

    posPanparametros2 = [.435 .15 .22 .295];
    pos               = pfig.*posPanparametros2;
    posCriteriosTitol = [10 pos(4)*0.7487 pos(3)*0.9 pos(4)*0.1157];

    panparametros2 = uipanel(...
        'TitlePosition','lefttop','Position',posPanparametros2,'BackgroundColor',colfig);
        panCriteriosTitol  = uicontrol('parent',panparametros2,'Style','text','String','Criterios de Selecci�n','FontSize',max(7,posCriteriosTitol(4)*0.5882),...%9*coefX,...
            'Position',posCriteriosTitol,'ForegroundColor','white','BackgroundColor',colfig,... %[15*coefX,110*coefY,170*coefX,17*coefY]
            'HorizontalAlignment','Left','FontWeight','Bold');

    posPanparametros3    = [.652 .15 .19 .295];
    pos                  = pfig.*posPanparametros3;
    posOptimizadorTitol  = [10 pos(4)*0.7487 pos(3)*0.9 pos(4)*0.1157];
    posOptimTitol        = [10 pos(4)*0.54 pos(3)*0.9 pos(4)*0.1157];
    posGeneracionesTitol = [10 pos(4)*0.31 pos(3)*0.9 pos(4)*0.1157];
    posIteracionesTitol  = [10 pos(4)*0.12 pos(3)*0.9 pos(4)*0.1157];

    panparametros3 = uipanel(...
        'TitlePosition','lefttop','Position',posPanparametros3,'BackgroundColor',colfig);
        panOptimizadorTitol  = uicontrol('parent',panparametros3,'Style','text','String','Optimizador','FontSize',max(7,posCriteriosTitol(4)*0.5882),...%9*coefX,...
            'Position',posOptimizadorTitol,'ForegroundColor','white','BackgroundColor',colfig,... %[15*coefX,110*coefY,170*coefX,17*coefY]
            'HorizontalAlignment','Left','FontWeight','Bold');
        panOptimTitol  = uicontrol('parent',panparametros3,'Style','text','String','Optimizador','FontSize',9*coefX,...
            'Position',posOptimTitol,'ForegroundColor','white','BackgroundColor',colfig,... %[15*coefX,80*coefY,150*coefX,17*coefY]
            'HorizontalAlignment','Left');
        panGeneracionesTitol  = uicontrol('parent',panparametros3,'Style','text','String','Generaciones','FontSize',9*coefX,...
            'Position',posGeneracionesTitol,'ForegroundColor','white','BackgroundColor',colfig,... %[15*coefX,50*coefY,150*coefX,17*coefY]
            'HorizontalAlignment','Left');
        panIteracionesTitol  = uicontrol('parent',panparametros3,'Style','text','String','Iteraciones','FontSize',9*coefX,...
            'Position',posIteracionesTitol,'ForegroundColor','white','BackgroundColor',colfig,... %[15*coefX,20*coefY,150*coefX,17*coefY]
            'HorizontalAlignment','Left');
     
    posPanreinicio = [.849 .15 .14 .295];
    pos                  = pfig.*posPanreinicio;
    posReinicioTitol = [10 pos(4)*0.9257 pos(3)*0.5 pos(4)*0.1157];

    panreinicio = uipanel(...
        'TitlePosition','lefttop','Position',posPanreinicio,'BackgroundColor',colfig);
        panReinicioTitol  = uicontrol('parent',panreinicio,'Style','text','String','Reinicio','FontSize',max(7,posCriteriosTitol(4)*0.58),...%10*coefX,...
            'Position',posReinicioTitol,'BackgroundColor','white',... %[10*coefX,136*coefY,60*coefX,17*coefY]
            'HorizontalAlignment','Center');

    % ------------------ Info resum ------------------
    entradaValor  = uicontrol('parent',panparametros0,'Style','text','String','-','FontSize',10*coefX,... %aiguaEntrada
            'Position',[15*coefX,60*coefY,180*coefX,30*coefY],'BackgroundColor',colfig,'ForegroundColor','white',...
            'HorizontalAlignment','left');
    salidaValor  = uicontrol('parent',panparametros0,'Style','text','String','-','FontSize',10*coefX,... %aiguaSortida
            'Position',[15*coefX,10*coefY,180*coefX,30*coefY],'BackgroundColor',colfig,'ForegroundColor','white',...
            'HorizontalAlignment','left');
    %---------------
    pos                   = pfig.*posPanparametros1;
    posQminValor          = [pos(1)*3.5 pos(4)*0.5526 pos(3)*0.3 pos(4)*0.1157];
    posQmaxValor          = [pos(1)*3.5 pos(4)*0.39 pos(3)*0.3 pos(4)*0.1157];
    posSmaxValor          = [pos(1)*3.85 pos(4)*0.23 pos(3)*0.25 pos(4)*0.1157];
    posTimplantacionValor = [pos(1)*4.28 pos(4)*0.075 pos(3)*0.19 pos(4)*0.1157];
    cabMinValor  = uicontrol('parent',panparametros1,'Style','text','String',cabalMin,'FontSize',max(8,posQminValor(4)*0.5882),...%10*coefX,...
            'Position',posQminValor,'BackgroundColor',colfig,'ForegroundColor','white',... %[115*coefX,82*coefY,70*coefX,15*coefY]
            'HorizontalAlignment','right'); %[330,163,70,15]
    cabMaxValor  = uicontrol('parent',panparametros1,'Style','text','String',cabalMax,'FontSize',max(8,posQmaxValor(4)*0.5882),...%10*coefX,...
            'Position',posQmaxValor,'BackgroundColor',colfig,'ForegroundColor','white',... %[115*coefX,59*coefY,70*coefX,15*coefY]
            'HorizontalAlignment','right'); %[330,138,70,15]
    supValor  = uicontrol('parent',panparametros1,'Style','text','String',superficie,'FontSize',max(8,posSmaxValor(4)*0.5882),...%10*coefX,...
            'Position',posSmaxValor,'BackgroundColor',colfig,'ForegroundColor','white',... %[115*coefX,36*coefY,70*coefX,15*coefY]
            'HorizontalAlignment','right'); %[330,113,70,15]
    tempsImpValor  = uicontrol('parent',panparametros1,'Style','text','String',tempsImplantacio,'FontSize',max(8,posTimplantacionValor(4)*0.5882),...%10*coefX,...
            'Position',posTimplantacionValor,'BackgroundColor',colfig,'ForegroundColor','white',... %[130*coefX,13*coefY,55*coefX,15*coefY]
            'HorizontalAlignment','right'); %[345,88,55,15]
    %--------------- 
    optimValor  = uicontrol('parent',panparametros3,'Style','text','String','Alg. Gen�ticos','FontSize',9*coefX,...
            'Position',[90*coefX,80*coefY,98*coefX,17*coefY],'ForegroundColor','white','BackgroundColor',colfig,...
            'HorizontalAlignment','Right');
    generacionsValor  = uicontrol('parent',panparametros3,'Style','text','String',generacions,'FontSize',10*coefX,...
            'Position',[115*coefX,50*coefY,73*coefX,15*coefY],'BackgroundColor',colfig,'ForegroundColor','white',...
            'HorizontalAlignment','right');
    iteracionsValor  = uicontrol('parent',panparametros3,'Style','text','String',iteracions,'FontSize',10*coefX,...
            'Position',[115*coefX,20*coefY,73*coefX,15*coefY],'BackgroundColor',colfig,'ForegroundColor','white',...
            'HorizontalAlignment','right');

    if ~isempty(criteris)
        critAux = criteris{1};
    else
        critAux = '-';
    end
    criteriosPopup  = uicontrol('parent',panparametros2,'Style','listbox','String',critAux,'FontSize',10*coefX,...
            'Position',[5*coefX,5*coefY,220*coefX,100*coefY],'BackgroundColor',colfig,'ForegroundColor','white',...
            'HorizontalAlignment','left');

%     criterisTriats = '';
%         textInd  = uicontrol('Parent',pan3,'Style','listbox',...
%             'String',criterisDisponibles,'Max',20,'Min',0,'Value',[],...
%             'Position',[15,15,(hpPos(3)*pfigC(3)-40)/2.5 pfigC(4)*hpPos(4)*0.9]);
        
    % ------------------ botons ------------------
    butInput = uicontrol('parent',panDatos,'Style', 'pushbutton', 'String', 'Cargar datos','FontSize',8*coefX,...
        'Position', [14*coefX 60*coefY 106*coefX 49*coefY], 'Callback', {@input_Callback});
    dataLed = uicontrol('parent',panDatos,'Style', 'text','Background',[1 0 0],...
        'Position', [60*coefX 20*coefY 20*coefX 20*coefY],'Tag','dataNok');
    
    entradesSortidesBut = uicontrol('parent',panconfiguracion,'Style', 'pushbutton', 'String', 'Aguas de entrada y salida','Enable','off',...
    'Position', [14*coefX 60*coefY 150*coefX 50*coefY],'FontSize',8*coefX,'Callback', {@entradesSortidesBut_Callback});
     entradesSortidesLed = uicontrol('parent',panconfiguracion,'Style', 'text','Background',[1 0 0],...
        'Position', [80*coefX 20*coefY 20*coefX 20*coefY],'Tag','dataNok');
    
    plantaBut = uicontrol('parent',panconfiguracion,'Style', 'pushbutton', 'String', 'Restricciones','FontSize',8*coefX,'Enable','off',...
    'Position', [182*coefX 60*coefY 110*coefX 50*coefY], 'Callback', {@plantaBut_Callback});
     plantaLed = uicontrol('parent',panconfiguracion,'Style', 'text','Background',[1 0 0],...
        'Position', [230*coefX 20*coefY 20*coefX 20*coefY],'Tag','dataNok');

    criterisBut = uicontrol('parent',panconfiguracion,'Style', 'pushbutton','String','Criterios','Enable','off','FontSize',8*coefX,...
    'Position', [310*coefX 60*coefY 110*coefX 50*coefY], 'Callback', {@criterisBut_Callback});
    criterisLed = uicontrol('parent',panconfiguracion,'Style', 'text','Background',[1 0 0],...
        'Position', [360*coefX 20*coefY 20*coefX 20*coefY],'Tag','dataNok');

    optimizadorBut = uicontrol('parent',panconfiguracion,'Style', 'pushbutton', 'String', 'Optimizador','Enable','off','FontSize',8*coefX,...
    'Position', [438*coefX 60*coefY 110*coefX 50*coefY], 'Callback', {@optimitzadorBut_Callback});
    optimizadorLed = uicontrol('parent',panconfiguracion,'Style', 'text','Background',[1 0 0],...
        'Position', [485*coefX 20*coefY 20*coefX 20*coefY],'Tag','dataNok');

    run = uicontrol('parent',panoptimizacion,'Style', 'pushbutton', 'String', 'Resolver Configuraci�n','Enable','off','FontSize',8*coefX,...
    'Position', [14*coefX 60*coefY 128*coefX 50*coefY], 'Callback', {@run_Callback});
    runLed = uicontrol('parent',panoptimizacion,'Style', 'text','Background',[1 0 0],...
        'Position', [70*coefX 20*coefY 20*coefX 20*coefY],'Tag','dataNok');

    exportBut = uicontrol('parent',panresultados,'Style', 'pushbutton', 'String', 'Abrir Resultados','Enable','off','FontSize',8*coefX,...
    'Position', [14*coefX 60*coefY 115*coefX 50*coefY], 'Callback', {@ExportExcel_Callback});
    exportLed = uicontrol('parent',panresultados,'Style', 'text','Background',[1 0 0],...
        'Position', [65*coefX 20*coefY 20*coefX 20*coefY],'Tag','dataNok');

    butResetParametres = uicontrol('parent',panreinicio,'Style', 'pushbutton', 'String', 'Reiniciar Par�metros','FontSize',8*coefX,...
        'Position', [14*coefX 45*coefY 115*coefX 50*coefY],'Enable','off','Callback', {@resetParametros_Callback});
    
    butInfo = uicontrol('Style', 'pushbutton', 'String', 'M�s informaci�n del proyecto �taca ...',...
        'FontSize',10*coefX,'Position', [10*coefX 10*coefY 280*coefX 30*coefY],'Callback',{@info_Callback},...
        'ForegroundColor','white','BackgroundColor',[51/255 51/255 51/255],...
        'FontWeight','Bold');
   
    set(f,'Name',' ITACA - Sistema de Gesti�n Avanzada')
    set(f,'Visible','on');
    movegui(f,'center')
    
%% LECTURA DADES DE L'EXCEL
    function input_Callback(source,eventdata)
        
        hlp = dialog('name','Datos Externos','Position',[360,400,300,70]);
            hlplbl  = uicontrol('parent',hlp,'Style','text',...
                'String','Cargando datos ...','Position',[(300-200)/2,70/5,200,40],'FontSize',15);
        movegui(hlp,'center');
        
        [filename, pathname] = uigetfile({'*.xlsx';'*.xls';'*.*'},'Selecciona un archivo');
        if isequal(filename,0)
            close(hlp);
            return
        end

        numData      = [];
        rawData      = [];
        txtData      = [];
        criteris     = [];
        criteri      = 0;
        criteriVisualitzacio = 1;
        indicadors   = [];
        indList      = cell(1,2);
        contaminants = cell(1,1);
        Cin          = cell(1,1);
        Cout         = cell(1,1);
        aiguaEntrada = 0;
        aiguaSortida = 0;
        solucio      = [];
        set(entradesSortidesLed,'Background',[1 0 0],'Tag','dataNok');
        set(plantaLed,'Background',[1 0 0],'Tag','dataNok');
        set(criterisLed,'Background',[1 0 0],'Tag','dataNok');
        set(exportLed,'Background',[1 0 0],'Tag','dataNok');
        set(optimizadorLed,'Background',[1 0 0],'Tag','dataOk');
        
        set(entradaValor,'String','-');
        set(salidaValor,'String','-');
        set(cabMinValor,'String','0');
        set(cabMaxValor,'String','0');
        set(supValor,'String','0');
        set(tempsImpValor,'String','0');
        set(criteriosPopup,'String','-');
        
        try
        [numData,  txtData,  rawData]   = xlsread([pathname filename],'Datos Tratamientos');%ADASA-Datos
        [numCin,   txtCin,   rawCin]    = xlsread([pathname filename],'Aguas entrada');
        [numCout,  txtCout,  rawCout]   = xlsread([pathname filename],'Aguas salida');
        catch me
            h = errordlg(me.message,'Error al abrir el fichero excel');
            return;
        end

        close(hlp);
        camp = 0;
        for j=2:length(txtData(1,:))
            if(length(txtData{1,j}))
                camp = camp+1;
            end
        end
%         %Par�metres per la Planta (cabal min, max, superf�cie i temps implantaci�)
%         paramsPlanta = numData(5:end,1:4);
        
        %Concentracions admisibles
        contaminants{1,1} = txtCin(3:end,1);
        criteris = cell(1+camp-4-length(contaminants{1}),1); %(2)En resto 6 que son les cabal max min, sup, temps, concentracions i reduccions
        crit = 1;
        ind  = 0;
        
        for i=(6+3*length(contaminants{1})):size(txtData,2) %6: cabalmin,max,sup,temps,conc ad,reduc
            if ~isempty(txtData{1,i})
                criteris{1,1} = [criteris{1,1};txtData(1,i)];
                crit = crit+1;
                ind = 0;
            end
            criteris{crit,1} = [criteris{crit,1};txtData(3,i)];
        end
        out = criteris;
        
        %Elimino els tractaments que no tenen dades de Cabal m�xim/m�nim
        [a,dummy]      = find(isnan(numData(5:end,1:2)));
        a = unique(a);
        if ~isempty(a)
            f = warndlg({'Atenci�n: Los siguientes tratamientos no ser�n considerados por falta de datos en los campos "Caudal m�ximo" y/o "Caudal m�nimo":',' ',rawData{a+5,1}}, 'Tratamientos: Falta de datos');
            numData(a+4,:) = [];
            rawData(a+5,:) = [];
            txtData(a+5,:) = [];
        end
                
        %Poso a 0 els NaNs de la superf�cie i temps d'Implantadci�
        [a,b]      = find(isnan(numData(5:end,3:4)));
        for i=1:length(a)
            numData(a(i)+4,b(i)+2) = 0;
        end
        
        %Poso un valor molt alt on falten dades de la concentraci�
        %admisible
        for i=5:3:3*length(contaminants{1})+6
            [amax,bmax]      = find(isnan(numData(5:end,i)));
            numData(amax+4,i)= 9999999;%1e10;
            [amax,bmax]      = find(isnan(numData(5:end,i+1)));
            numData(amax+4,i+1) = 0;
        end
        %Inverteixo el percentatge de reduccio del 100% com a valor m�xim
        %al 0 com a valor m�xim i del 0% com a valor m�nim a l'1 com a
        %valor m�nim
        offset = length(contaminants{1});
        numData(5:end,7:3:(3*offset+6))=...
            1-numData(5:end,7:3:(3*offset+6))/100;
        
        %Poso a 0 els indicadors sense valor
        [a,b]      = find(isnan(numData(5:end,3*offset+5:end)));
        for i=1:length(a)
            numData(a(i)+4,b(i)+3*offset+4) = 0;
        end

        %Igualo des dimensions dels vectors de les aig�es d'E/S        
        numCin  = dimensions(numCin,length(contaminants{1}),size(txtCout,2)-2);
        numCout = dimensions(numCout,length(contaminants{1}),size(txtCout,2)-2);
        
        Cin  = txtCin(1:2,3:end);
        Cout = txtCout(1:2,3:end);
        
        set(dataLed,'Background',[0 1 0],'Tag','dataOk');
        set(entradesSortidesLed,'Tag','dataOk');
        set(entradesSortidesBut,'Enable','on');
        set(plantaLed,'Tag','dataOk');
        set(plantaBut,'Enable','on');
        set(criterisLed,'Tag','dataOk');
        set(criterisBut,'Enable','on');
        set(optimizadorLed,'Background',[0 1 0],'Tag','dataOk');
        set(optimizadorBut,'Enable','on');
        set(runLed,'Tag','dataOk');
        set(exportLed,'Tag','dataOk');
        set(butResetParametres, 'Enable','on');
    end

%% INICIA LA OPTIMITZACIO -> RUN
    function run_Callback(source,eventdata)
        if(isempty(criteris))
            warndlg({'No hay datos. Realice la carga de datos de la correspondiente hoja de datos'},'ATENCION');
        elseif(isempty(indList{1,1}))
            warndlg({'No hay indicadores selecionados. Elija un criterio y selecione el/los indicadores deseado/s.'},'ATENCION');
        elseif(length(Cin)==0 || length(Cout)==0)
            warndlg('No se han seleccionado alguna de las concentraciones de entrada o salida deseada','ATENCION');
        else
            disp(' ');
            disp(' ');
            disp('*******************************');
            disp('**** S''INICIA OPTIMITZACI� ****');
            disp('*******************************');

            a = tractamentsCandidats;
            if isempty(a)
                errordlg('ATECI�N: No se han encontrado tratamientos que cumplan con las condiciones de Planta introducidas','Criterios de Planta','modal');
                return
            end

            %Criteris
            aux0 = 1:length(txtData(6:end,1));

            aux0(a) = [];

            resum         = dialog('WindowStyle', 'modal',...
                               'Name','Tratamientos considerados','Position',[15 70 400 600],...
                               'Color',colfig);
            posResum = get(resum,'position');

            compatiblesLabel = uicontrol('Style','text',...
                'String','Tratamientos compatibles con la configuraci�n deseada:',...
                'FontSize',10,'ForegroundColor','white','BackgroundColor',colfig,...
                'HorizontalAlignment','Left','Position',[10 posResum(4)-25 posResum(3)-20 17]);
            listCompatibles = uicontrol('Style','listbox',... %'Parent',resum,
                'String',rawData(a+5,1),'SelectionHighlight','off',...
                'Position',[10 posResum(4)*2/3+40 posResum(3)-20 posResum(4)/3-70]);%-60-250

            noCompatiblesLabel = uicontrol('Style','text',...
                'String','Tratamientos NO compatibles con la configuraci�n deseada:',...
                'FontSize',10,'ForegroundColor','white','BackgroundColor',colfig,...
                'HorizontalAlignment','Left','Position',[10 posResum(4)*2/3-10 posResum(3)-20 17]);
            listNoCompatibles = uicontrol('Style','listbox',...
                'String',rawData(aux0+5,1),'SelectionHighlight','off',...
                'Position',[10 posResum(4)/3+40 posResum(3)-20 posResum(4)/3-70]);

            parametresLabel = uicontrol('Style','text',...
                'String','Par�metros de optimizaci�n posible:',...
                'FontSize',10,'ForegroundColor','white','BackgroundColor',colfig,...
                'HorizontalAlignment','Left','Position',[10 posResum(4)/3-10 posResum(3)-20 17]);
            listParametres = uicontrol('Style','listbox',...
                'String',contaminants{1}(componentsAOptimitzar),'SelectionHighlight','off',...
                'Position',[10 50 posResum(3)-20 posResum(4)/3-70]);

            okResumBut     = uicontrol('Parent',resum,'Style', 'pushbutton', 'String', 'Continuar',...
                'Position', [10 10 100 30],...
                'Tag','Resum','Callback', {@okBut_Callback});
            cancelResumBut = uicontrol('Parent',resum,'Style', 'pushbutton', 'String', 'Cancelar',...
                'Position', [posResum(3)-110 10 100 30],...
                'Tag','Resum','Callback', {@cancelBut_Callback});

            movegui(resum,'center');
        end
    end

%% ========================================================================

%% BOTO RESET PARAMETRES
    function resetParametros_Callback(soucrce,eventdata)
        
        criteri      = 0;
        criteriVisualitzacio = 1;
        indicadors   = [];
        indList      = cell(1,2);
        aiguaEntrada = 0;
        aiguaSortida = 0;
        solucio      = [];
        
%         set(entradesSortidesLed,'Value',0);
%         set(plantaLed,'Value',0);
%         set(criterisLed,'Value',0);
%         set(exportLed,'Value',0);
        set(entradesSortidesLed,'Background',[1 0 0]);
        set(plantaLed,'Background',[1 0 0]);
        set(criterisLed,'Background',[1 0 0]);
        set(exportLed,'Background',[1 0 0]);
        set(runLed,'Background',[1 0 0]);
        set(run,'Enable','off');
        
        set(entradaValor,'String','-');
        set(salidaValor,'String','-');
        set(cabMinValor,'String','0');
        set(cabMaxValor,'String','0');
        set(supValor,'String','0');
        set(tempsImpValor,'String','0');
        set(criteriosPopup,'String','-');
    end

%% BOTO CRITERIS
function criterisBut_Callback(source,eventdata)

    indListCriteris    = indList;
    indicadorsCriteris = indicadors;
    
    c = figure('NumberTitle','off','Visible','off','Position',[360,400,650,450],...
        'menubar','none','Toolbar','none','Resize','off','WindowStyle','modal', ...
        'Color',colfig);

    pfigC   = get(c,'position');
    
    panSelCriterios = uipanel(... %'Title','Seleccion Criterios','FontSize',10,
        'Position',[.02 .78 .96 .17],'BackgroundColor',colfig);
        selCriteriosTitol = uicontrol('Style','text','String','Selecci�n de Criterios',...
        'FontSize',10,'HorizontalAlignment','Center','BackgroundColor','white',...
        'Position',[20,420,150,17]);
    
    pan3 = uipanel(... %'Title','Indicadores seleccionados','FontSize',10,
        'Position',[.02 .11 .96 .63],'BackgroundColor',colfig);
        indicadoresTitol = uicontrol('Style','text','String','Indicadores seleccionados',...
        'FontSize',10,'HorizontalAlignment','Center','BackgroundColor','white',...
        'Position',[20,324,170,17]);
    
    hpPos = get(pan3,'Position');
    
    if ~isempty(criteris)
        criterisLlista = criteris{1};
    else
        criterisLlista = 'No hay datos';
    end
    IndicadorsPopup = uicontrol('Style','popupmenu',...
        'String',criterisLlista,'FontSize',10,...
        'Position',[30,380,590,20],...
        'Callback',{@IndicadorsMenuPopup_Callback});
    
    if ~isempty(indListCriteris)
        indicadorsLlista = indListCriteris(:,1);
    else
        indicadorsLlista = 'No hay datos';
    end
    textAgreg  = uicontrol('Parent',pan3,'Style','listbox',...
        'String',indicadorsLlista,'Max',2,'Min',0,'Value',[],...
        'Position',[20,75,hpPos(3)*pfigC(3)-40,hpPos(4)*pfigC(4)-100]);
         
    txtPos = get(textAgreg,'Position');

    indFiltro  = uicontrol('Parent',pan3,'Style','text','String','Filtro de Indicadores seg�n Criterios',...
        'FontSize',10,'HorizontalAlignment','Left','ForegroundColor','white',...
        'Position',[20,45,txtPos(3),20],'BackgroundColor',colfig);
    
    CriterisPopup = uicontrol('Parent',pan3,'Style','popupmenu','String',['Todos los criterios (*)';criteris{1}],...
        'FontSize',10,'Position',[20,25,txtPos(3),20],'Callback',{@CriterisMenuPopup_Callback});

    okBut = uicontrol('Style', 'pushbutton', 'String', 'Aceptar',...
    'Position', [15 10 70 30],'Tag','Criteris', 'Callback', {@okBut_Callback});
    cancelBut = uicontrol('Style', 'pushbutton', 'String', 'Cancelar',...
    'Position', [pfigC(3)-80 10 70 30],'Tag','Criteris', 'Callback', {@cancelBut_Callback});

    set(c,'Name','Criteriose e indicadores')
    set(c,'Visible','on');
    movegui(c,'center')
end

%% BOTO AIGUA ENTRADA/SORTIDA
function entradesSortidesBut_Callback(source,eventdata)
    ea = figure('NumberTitle','off','Visible','off','Position',[360,200,300,400],...
        'menubar','none','Toolbar','none','Resize','off','WindowStyle','normal',...
        'Color',colfig);

    pfigEA   = get(ea,'position');
    colfigEA = get(ea,'Color');
    
    panelEntrada = uipanel(... ,'Title','Aguas de entrada y salida','FontSize',10,
        'Position',[.03 .55 .94 .41],'BackgroundColor',colfigEA);
        panaiguaSortTitol  = uicontrol('Style','text','String','Agua de entrada','FontSize',10,...
            'Position',[20,375,100,17],'BackgroundColor','white',...
            'HorizontalAlignment','Center');  %,'ForegroundColor','white'); colfigEA
        
    panelSortida = uipanel(... %'Title','Aguas de entrada y salida','FontSize',10,
        'Position',[.03 .12 .94 .39],'BackgroundColor',colfigEA);
        panaiguaSortTitol  = uicontrol('Style','text','String','Agua de salida','FontSize',10,...
            'Position',[20,195,100,17],'BackgroundColor','white',...
            'HorizontalAlignment','Center');
        
    aiguaEntradaPop = uicontrol('Style','popupmenu','String',Cin(1,:),...%'No hay datos'
            'Position',[20,340,260,25],'Callback',{@aiguaEntradaPopup_Callback});
        if aiguaE==0, descE = '-'; else descE = Cin(2,aiguaE); end;
        descripEntradaTxt = uicontrol('Style','text','String',descE,...
            'FontSize',10,'Position',[20,230,260,100],'ForegroundColor','white',...
            'BackgroundColor',colfig,'HorizontalAlignment','Left');
    aiguaSortidaPop = uicontrol('Style','popupmenu','String',Cout(1,:),...
            'Position',[20,160,260,25],'Callback',{@aiguaSortidaPopup_Callback});
        if aiguaS==0, descS = '-'; else descS = Cout(2,aiguaS); end;
        descripSortidaTxt = uicontrol('Style','text','String',descS,...
            'FontSize',10,'Position',[20,55,260,100],'ForegroundColor','white',...
            'BackgroundColor',colfig,'HorizontalAlignment','Left');
        
        
    okAiguaBut = uicontrol('Style', 'pushbutton', 'String', 'Aceptar',...
            'Position', [10 10 70 30],'Tag','Aigua', 'Callback', {@okBut_Callback});
    cancelAiguaBut = uicontrol('Style', 'pushbutton', 'String', 'Cancelar',...
            'Position', [(pfigEA(3)-78) 10 70 30],'Tag','Aigua', 'Callback', {@cancelBut_Callback});

    set(ea,'Name','AGUA DE ENTRADA Y SALIDA')
    set(ea,'Visible','on');
    movegui(ea,'center')
end

%% BOTO PLANTA
function plantaBut_Callback(source,eventdata)

    cabalMinPlanta         = cabalMin;
    cabalMaxPlanta         = cabalMax;
    superficiePlanta       = superficie;
    tempsImplantacioPlanta = tempsImplantacio;
    numEtapesPlanta        = numEtapes;
    
    p = figure('NumberTitle','off','Visible','off','Position',[360,400,360,300],...
        'menubar','none','Toolbar','none','Resize','off','WindowStyle','modal',...
        'Color', colfig);

    pfigP   = get(p,'position');
        
    panel     = uipanel(... %'Title','Parametros de configuraci�n','FontSize',10,
        'Position',[.02 .18 .96 .72],'BackgroundColor',colfig);
        panTitol  = uicontrol('Style','text','String','Parametros de configuraci�n',...
            'Position',[20,260,150,17],'HorizontalAlignment','Center',...
            'BackgroundColor','white');
    
    cabalMinlabel  = uicontrol('Style','text','String','Caudal m�nimo                                              m3/s',...
            'FontSize',10,'Position',[20,200,330,25],'HorizontalAlignment','left',...
            'BackgroundColor',colfig,'ForegroundColor','white');
    cabalMinEdit  = uicontrol('Style','edit','Min',0,'Position',[170,205,100,25],...
            'string',num2str(cabalMin),'Callback',{@cabalMin_Callback});
        
    cabalMaxlabel  = uicontrol('Style','text','String','Caudal m�ximo                                             m3/s',...
            'FontSize',10,'Position',[20,170,330,25],'HorizontalAlignment','left',...
            'BackgroundColor',colfig,'ForegroundColor','white');
    cabalMaxEdit  = uicontrol('Style','edit','Min',0,'Position',[170,175,100,25],...
            'string',num2str(cabalMax),'Callback',{@cabalMax_Callback});

    tempsImplantaciolabel  = uicontrol('Style','text','String','Tiempo de Implantaci�n                                semanas',...
            'FontSize',10,'Position',[20,140,330,25],'HorizontalAlignment','left',...
            'BackgroundColor',colfig,'ForegroundColor','white');
    tempsImplantacioEdit  = uicontrol('Style','edit','Min',0,'Position',[170,145,100,25],...
            'string',num2str(tempsImplantacio),'Callback',{@tempsImplantacio_Callback});
        
    superficielabel  = uicontrol('Style','text','String','Superf�cie                                                     m2',...
            'FontSize',10,'Position',[20,110,330,25],'HorizontalAlignment','left',...
            'BackgroundColor',colfig,'ForegroundColor','white');
    superficieEdit  = uicontrol('Style','edit','Min',0,'Position',[170,115,100,25],...
            'string',num2str(superficie),'Callback',{@superficie_Callback});
        
    numEtapeslabel  = uicontrol('Style','text','String','N�mero de etapas',...
            'FontSize',10,'Position',[20,80,330,25],'HorizontalAlignment','left',...
            'BackgroundColor',colfig,'ForegroundColor','white');
    numEtapesEdit  = uicontrol('Style','edit','Min',0,'Position',[170,85,100,25],...
            'string',num2str(numEtapes),'Callback',{@nombreEtapes_Callback});
    
    okBut = uicontrol('Style', 'pushbutton', 'String', 'Aceptar',...
    'Position', [10 10 70 30],'Tag','Planta','Callback', {@okBut_Callback});
    cancelBut = uicontrol('Style', 'pushbutton', 'String', 'Cancelar',...
    'Position', [(pfigP(3)-76) 10 70 30],'Tag','Planta','Callback', {@cancelBut_Callback});

    set(p,'Name','Restricciones')
    set(p,'Visible','on');
    movegui(p,'center')
end

%% BOTO OPTIMITZADOR
function optimitzadorBut_Callback(source,eventdata)
    
    iteracionsOptimitzador  = iteracions;
    switch(optimitzador)
        case 'genetics'
            generacionsOptimitzador = generacions;
        case 'pattern'
            generacionsOptimitzador = iteracionsLimit;
    end
    
    o = figure('NumberTitle','off','Visible','off','Position',[360,200,300,250],...
        'menubar','none','Toolbar','none','Resize','off','WindowStyle','modal',...
        'Color',colfig);

    pfigO   = get(o,'position');
    
    panel     = uipanel(... %'Title','Par�metros de optimizador','FontSize',10,
        'Position',[.03 .2 .93 .72],'BackgroundColor',colfig);
        panTitol  = uicontrol('Style','text','String','Par�metros de optimizador','FontSize',10,...
            'Position',[20,222,200,17],'BackgroundColor','white',...
            'HorizontalAlignment','Center');  %,'ForegroundColor','white'); colfigEA

    optimizadorlabel  = uicontrol('Style','text','String','Optimizador',...
             'FontSize',10,'Position',[20,165,200,20],'HorizontalAlignment','left',...
            'ForegroundColor','white','BackgroundColor',colfig);
    optimizadorPop = uicontrol('Style','popupmenu','String',{'Algoritmos Gen�ticos','Pattern Search'},...
            'Position',[130,165,150,20],'Callback',{@optimizadorPopup_Callback});
        
    iteracionslabel  = uicontrol('Style','text','String','N�mero de Iteraciones',...
             'FontSize',10,'Position',[20,125,200,20],'HorizontalAlignment','left',...
            'ForegroundColor','white','BackgroundColor',colfig);
    iteracionsEdit  = uicontrol('Style','edit','Min',0,'Position',[180,125,100,20],...
            'string',num2str(iteracions),'Callback',{@iteracions_Callback});
    
    generacionslabel  = uicontrol('Style','text','String','N�mero de Generaciones',...
             'FontSize',10,'Position',[20,85,200,20],'HorizontalAlignment','left',...
            'ForegroundColor','white','BackgroundColor',colfig);
%     generacionsEdit  = uicontrol('Style','edit','Min',0,'Position',[180,85,100,20],...
%             'string',num2str(generacions),'Callback',{@generacions_Callback});
    generacionsEdit  = uicontrol('Style','edit','Min',0,'Position',[180,85,100,20],...
            'string',num2str(generacionsOptimitzador),'Callback',{@generacions_Callback});
        
    okOptimitzadorBut = uicontrol('Style', 'pushbutton', 'String', 'Aceptar',...
    'Position', [10 10 70 30],'Tag','Optimitzador','Callback', {@okBut_Callback});
    cancelOptimitzadorBut = uicontrol('Style', 'pushbutton', 'String', 'Cancelar',...
    'Position', [(pfigO(1)-2*70) 10 70 30],'Tag','Optimitzador','Callback', {@cancelBut_Callback});

    if strcmp(optimitzador,'pattern')
        set(optimizadorPop,'Value',2);
%         set(iteracionsEdit,'Enable','off');
%         set(generacionsEdit,'Enable','off');
%        set(generacionslabel,'String','Numero de Iteraciones');
    else
        set(optimizadorPop,'Value',1);
%         set(iteracionsEdit,'Enable','on');
%         set(generacionsEdit,'Enable','off');
%        set(generacionslabel,'String','N�mero de Generaciones');
    end
    
    set(o,'Name','Optimizador')
    set(o,'Visible','on');
    movegui(o,'center')
end

%% BOTO INFO
function info_Callback(source,eventdata)

    infoFig = figure('NumberTitle','off','Visible','off','Position',[360,400,870,550],...%650,485],...
        'menubar','none','Toolbar','none','Resize','off');
    processImatge = imread('info.png');
    Z = imresize(processImatge,5);
    imshow(Z,'Border','tight','InitialMagnification','fit');%'Parent', imgca(f),
    
    okInfoBut = uicontrol('Style', 'pushbutton', 'String', 'Aceptar',...
    'Position', [870/2-50 10 100 30],'Tag','Info','Callback', {@okBut_Callback});
    
    set(infoFig,'Name','Informaci�n')
    set(infoFig,'Visible','on');
    movegui(infoFig,'center')
end

%% EXPORTAR RESULTATS A EXCEL
    function ExportExcel_Callback(source,eventdata)

        titols      = {'Agua Entrada','Agua Salida','','Par�metros Planta';Cin{1,aiguaEntrada},Cout{1,aiguaSortida},'',''};% [numCin(:,aiguaEntrada), numCout(:,aiguaSortida)]};
        paramPlanta = {'Caudal m�nimo',cabalMin;'Caudal m�ximo',cabalMax;'Superf�cie',superficie;'Tiempo implantaci�n',tempsImplantacio};
        sheet       = datestr(now,'ddmmyyyy-HHMMSS');
        a           = tractamentsCandidats;
        aux         = rawData(a+5,:);
        [c, ia, ic] = unique(indList(:,2));
        
        xlswrite('resultados.xls', {'Resultados'}, sheet, 'A1');
        
        xlswrite('resultados.xls', {'1�'}, sheet, 'A4');
        xlswrite('resultados.xls', {'2�'}, sheet, 'F4');
        xlswrite('resultados.xls', {'3�'}, sheet, 'k4');
         
        for i=1:min(3,size(solucio,1))
            %   Tractaments
            [ro,co] = find(solucio(end-(i-1),:));
            xlswrite('resultados.xls', aux(solucio(end-(i-1),co),1), sheet, [char(66+5*(i-1)) '4']);%4+2+2
            % --- costos finals ---
            [costosImp, costosExp, ambient, prod, pesos] = CostosPesos(a,true);
            
            if ~isempty(costosImp)
                costFinal    = cumsum(costosImp(solucio(end-(i-1),co)));
                xlswrite('resultados.xls', {'COSTE FINAL:',costFinal(end),'�'}, sheet, [char(66+5*(i-1)) '14']);
            end
            if ~isempty(costosExp)
                costFinal    = cumsum(costosExp(solucio(end-(i-1),co)));
                xlswrite('resultados.xls', {'COSTE FINAL:',costFinal(end),'�'}, sheet, [char(66+5*(i-1)) '15']);
            end
            if ~isempty(ambient)
                ambientFinal = cumsum(ambient(solucio(end-(i-1),co)));
                xlswrite('resultados.xls', {'IMPACTO AMBIENTAL:',ambientFinal(end)}, sheet, [char(66+5*(i-1)) '16']);
            end
            if ~isempty(prod)
                prodFinal    = cumsum(prod(solucio(end-(i-1),co)));
                xlswrite('resultados.xls', {'PRODUCCION Y VALORIZACION DE SUBPRODUCTOS:',prodFinal(end)}, sheet, [char(66+5*(i-1)) '17']);
            end
        end
        
        xlswrite('resultados.xls', {'Condiciones Iniciales'}, sheet, 'A19');
        xlswrite('resultados.xls', titols, sheet, 'B20');
        xlswrite('resultados.xls', contaminants{:}, sheet, 'A22');
        xlswrite('resultados.xls', [numCinSeleccio(:,aiguaEntrada), numCoutSeleccio(:,aiguaSortida)], sheet, 'B22');
        xlswrite('resultados.xls', paramPlanta, sheet, 'E21');
        xlswrite('resultados.xls', {'Criterios /Indicadores'}, sheet, 'H19');
        xlswrite('resultados.xls', {'Costes Implantaci�n (�/UF.a�o)'}, sheet, 'H20');
        xlswrite('resultados.xls', {'Costes Explotaci�n (�/UF.a�o)'}, sheet, 'I20');
        xlswrite('resultados.xls', {'Impacto Ambiental'}, sheet, 'J20');
        xlswrite('resultados.xls', {'Valorizaci�n de subptoductos'}, sheet, 'K20');
        
        for i=1:length(c)
            
            if ~isempty(strfind(criteris{1}{str2num(c{i})},'Cost')) && ~isempty(strfind(criteris{1}{str2num(c{i})},'Impl'))
                xlswrite('resultados.xls', indList(find(strcmp(indList(:,2),c(i))),1), sheet, 'H21');% num2str(20+i)]
            elseif ~isempty(strfind(criteris{1}{str2num(c{i})},'Cost')) && ~isempty(strfind(criteris{1}{str2num(c{i})},'Expl'))
                xlswrite('resultados.xls', indList(find(strcmp(indList(:,2),c(i))),1), sheet, 'I21');
            elseif ~isempty(strfind(criteris{1}{str2num(c{i})},'Impact'))
                xlswrite('resultados.xls', indList(find(strcmp(indList(:,2),c(i))),1), sheet, 'J21');
            elseif ~isempty(strfind(criteris{1}{str2num(c{i})},'Prod'))
                xlswrite('resultados.xls', indList(find(strcmp(indList(:,2),c(i))),1), sheet, 'K21');
            end
        end
                
        xlswrite('resultados.xls', {'Datos Tratamientos'}, sheet, 'A41');
        xlswrite('resultados.xls', rawData(1:5,:), sheet, 'B42');

        xlswrite('resultados.xls', {'1�'}, sheet, 'A47');
        xlswrite('resultados.xls', {'2�'}, sheet, 'A58');
        xlswrite('resultados.xls', {'3�'}, sheet, 'A69');
        for i=1:min(3,size(solucio,1))
            [ro,co] = find(solucio(end-(i-1),:));
            xlswrite('resultados.xls', aux(solucio(end-(i-1),co),:), sheet, ['B' num2str(47+11*(i-1))]);
        end
    end

%% ------------------------------------------------------------------------
%%  ========  FUNCIONS DE LA SEL�LECCIO DE CRITERIS / INDICADORS  =========

%% SEL�LECCI� DELS INDICADORS
    function IndicadorsMenuPopup_Callback(source,eventdata)
        criteri = get(source, 'Value');
        if (isempty(criteris))
            return;
        end
        
        indListCriterisSeleccio = indListCriteris;
        %indicadorsCriterisSeleccio = indicadorsCriteris;
        
        ind = figure('NumberTitle','off','Visible','off','Position',[360,400,650,450],...
                'menubar','none','Toolbar','none','Resize','off','WindowStyle','modal',...
                'Color',colfig);

        pfigC   = get(ind,'position');
        
        pan3     = uipanel(... %'Title','Indicadores seleccionados','FontSize',10,
            'Position',[.02 .12 .96 .835],'BackgroundColor',colfig);
            seleccionTitol = uicontrol('Style','text','String','Indicadores seleccionados',...
                'FontSize',10,'HorizontalAlignment','Center','BackgroundColor','white',...
                'Position',[25,420,200,17]);
        
        hpPos = get(pan3,'Position');

        if ~isempty(criteris{criteri+1,1})
            criterisDisponibles = criteris{criteri+1,1};
        else
            criterisDisponibles = 'No hay datos..';
        end;
        criterisTriats = '';
        textInd  = uicontrol('Parent',pan3,'Style','listbox',...
            'String',criterisDisponibles,'Max',20,'Min',0,'Value',[],...
            'Position',[15,15,(hpPos(3)*pfigC(3)-40)/2.5 pfigC(4)*hpPos(4)*0.9]);
            %'Position',[15,15,(hpPos(3)*pfigC(3)-40)/2.5,pfig(4)*0.7]);
        
        indexSelected = strcmp(indListCriterisSeleccio(:,2),num2str(criteri));

        textSelected  = uicontrol('Parent',pan3,'Style','listbox',...
            'String',indListCriterisSeleccio(find(indexSelected==1),1),'Max',20,'Min',0,'Value',[],...
            'Position',[pfigC(3)-(hpPos(3)*pfigC(3)-0)/2.5-25,15,(hpPos(3)*pfigC(3)-40)/2.5,pfigC(4)*hpPos(4)*0.9]);
        
        addButton = uicontrol('Style', 'pushbutton', 'String', 'A�ade ->',...
             'Position', [pfigC(3)/2-25 pfigC(4)/2+50 70 50],...
             'Callback', {@addBut_Callback});
        rmvButton = uicontrol('Style', 'pushbutton', 'String', 'Elimina',...
             'Position', [pfigC(3)/2-25 pfigC(4)/2-50 70 50],...
             'Callback', {@rmvBut_Callback});

        okBut = uicontrol('Style', 'pushbutton', 'String', 'Aceptar',...
            'Position', [15 10 70 30],'Tag','Indicadors', 'Callback', {@okBut_Callback});
        cancelBut = uicontrol('Style', 'pushbutton', 'String', 'Cancelar',...
            'Position', [pfigC(3)-80 10 70 30],'Tag','Indicadors', 'Callback', {@cancelBut_Callback});

        set(ind,'Name','Selecci�n de indicadores')
        set(ind,'Visible','on');
        movegui(ind,'center')


    
    
%         [s,v] = listdlg('PromptString','Sel�lecciona Indicadores:',...
%                     'SelectionMode','multiple','ListSize',[300 300],...
%                     'ListString',criteris{criteri+1,1});
%         if v==1 %Si s'ha seleccionat algun element
%             trobat = 0;
%             for i=1:length(s)
%                 for j=1:length(indListCriteris)
%                     k = strcmp(criteris{criteri+1,1}(s(i),1),indListCriteris(j));
%                     if k
%                         trobat = j;
%                         continue;
%                     end
%                 end
%                 if ~trobat
%                     if isempty(indListCriteris) %indList{1,1}
%                         indListCriteris = [criteris{criteri+1,1}(s(i),1) {num2str(criteri)}];
%                     else
%                         if isempty(indListCriteris{1,1})
%                             indListCriteris = [criteris{criteri+1,1}(s(i),1) {num2str(criteri)}];
%                         else
%                             indListCriteris = [indListCriteris;criteris{criteri+1,1}(s(i),1) {num2str(criteri)}];
%                         end
%                     end
%                 else
%                     indListCriteris(trobat,:) = [];
%                 end
%                 trobat = 0;
%             end
%             
%             try
%                 if (criteriVisualitzacio==1)
%                     set(textAgreg,'String',indListCriteris(:,1));
%                 else
%                     index = strcmp(indListCriteris(:,2),num2str(criteriVisualitzacio-1));
%                     set(textAgreg,'String',indListCriteris(find(index==1),1));
%                 end
%                 %set(CriterisPopup,'String',['Todos los criterios (*)';criteris{1}]);
%             catch lasterror
%                 a=0;
%             end
% 
%             for i=3:(criteri+1)
%                 s = s+length(criteris{i-1,1});
%             end
%             indicadors = [indicadors s];
%         else %Si no s'ha seleccionat cap element (bot� cancel)
%             disp('No items selected');
%             return;
%         end
    end

%% FILTRE D'INDICADORS PER CRITERI
    function CriterisMenuPopup_Callback(source,eventdata)
        criteriVisualitzacio = get(source, 'Value');
        if (criteriVisualitzacio==1)
            set(textAgreg,'String',indListCriteris(:,1));
        else
            index = strcmp(indListCriteris(:,2),num2str(criteriVisualitzacio-1));
            set(textAgreg,'String',indListCriteris(find(index==1),1));
        end
    end
    function addBut_Callback(source,eventdata)
        resultat = get(textInd, 'Value');
        
        
        %         [s,v] = listdlg('PromptString','Sel�lecciona Indicadores:',...
%                     'SelectionMode','multiple','ListSize',[300 300],...
%                     'ListString',criteris{criteri+1,1});
        if ~isempty(resultat) %Si s'ha seleccionat algun element
            trobat = 0;
            for i=1:length(resultat)
                for j=1:length(indListCriterisSeleccio)
                    %k = strcmp(criteris{criteri+1,1}(resultat(i),1),indListCriteris(j));
                    if strcmp(criteris{criteri+1,1}(resultat(i),1),indListCriterisSeleccio(j));%k
                        trobat = j;
                        continue;
                    end
                end
                if ~trobat
                    if isempty(indListCriterisSeleccio) %indList{1,1}
                        indListCriterisSeleccio = [criteris{criteri+1,1}(resultat(i),1) {num2str(criteri)}];
                    else
                        if isempty(indListCriterisSeleccio{1,1})
                            indListCriterisSeleccio = [criteris{criteri+1,1}(resultat(i),1) {num2str(criteri)}];
                        else
                            indListCriterisSeleccio = [indListCriterisSeleccio;criteris{criteri+1,1}(resultat(i),1) {num2str(criteri)}];
                        end
                    end
                else
                    %indListCriteris(trobat,:) = [];
                end
                trobat = 0;
            end
            
            
            indexSelected = strcmp(indListCriterisSeleccio(:,2),num2str(criteri));
            set(textSelected,'String',indListCriterisSeleccio(find(indexSelected==1),1),'Value',[])
            if (criteriVisualitzacio==1)
                set(textAgreg,'String',indListCriterisSeleccio(:,1));
            else
                index = strcmp(indListCriterisSeleccio(:,2),num2str(criteriVisualitzacio-1));
                set(textAgreg,'String',indListCriterisSeleccio(find(index==1),1));
            end

            for i=3:(criteri+1)
                resultat = resultat+length(criteris{i-1,1});
            end
            indicadors = [indicadors resultat];
            
        else    %Si no s'ha seleccionat cap element (bot� cancel)
            disp('No items selected');
            return;
        end

    end
    function rmvBut_Callback(source,eventdata)
        resultat = get(textSelected, 'Value');
        indexSelected = strcmp(indListCriterisSeleccio(:,2),num2str(criteri));
        [fil,col] = find(indexSelected==1);
        indListCriterisSeleccio(fil(resultat),:) = [];
        indexSelected = strcmp(indListCriterisSeleccio(:,2),num2str(criteri));
        set(textSelected,'String',indListCriterisSeleccio(find(indexSelected==1),1),'Value',[]);
    end

%%  ===========  FUNCIONS DE LA ENTRADA I SORTIDA DE L'AIGUA  =============

%% AIGUA D'ENTRADA (AIGUA)
function aiguaEntradaPopup_Callback(hObject, eventdata, handles)
    aiguaE = get(hObject, 'Value');
    set(descripEntradaTxt,'String',Cin(2,aiguaE));
end
%% AIGUA DE SORTIDA (AIGUA)
function aiguaSortidaPopup_Callback(hObject, eventdata, handles)
    aiguaS = get(hObject, 'Value');
    set(descripSortidaTxt,'String',Cout(2,aiguaS));
end

%%  =====================  FUNCIONS DE LA PLANTA  =========================

%% CABAL M�NIM (PLANTA)
function cabalMin_Callback(hObject, eventdata, handles)
    user_entry = str2double(get(hObject,'string'));
    if isnan(user_entry)
        errordlg('You must enter a numeric value','Bad Input','modal')
        uicontrol(hObject)
        return
    end
    cabalMinPlanta = user_entry;
end
%% CABAL M�XIM (PLANTA)
function cabalMax_Callback(hObject, eventdata, handles)
    user_entry = str2double(get(hObject,'string'));
    if isnan(user_entry)
        errordlg('You must enter a numeric value','Bad Input','modal')
        uicontrol(hObject)
        return
    end
    cabalMaxPlanta = user_entry;
end
%% SUPERFICIE (PLANTA)
function superficie_Callback(hObject, eventdata, handles)
    user_entry = str2double(get(hObject,'string'));
    if isnan(user_entry)
        errordlg('You must enter a numeric value','Bad Input','modal')
        uicontrol(hObject)
        return
    end
    superficiePlanta = user_entry;
end
%% TEMPS D'IMPLANTACI� (PLANTA)
function tempsImplantacio_Callback(hObject, eventdata, handles)
    user_entry = str2double(get(hObject,'string'));
    if isnan(user_entry)
        errordlg('You must enter a numeric value','Bad Input','modal')
        uicontrol(hObject)
        return
    end
    tempsImplantacioPlanta = user_entry;
end
%% NOMBRE D'ETAPES (PLANTA)
function nombreEtapes_Callback(hObject, eventdata, handles)
    user_entry = str2double(get(hObject,'string'));
    if isnan(user_entry)
        errordlg('You must enter a numeric value','Bad Input','modal')
        uicontrol(hObject)
        return
    end
    numEtapesPlanta = user_entry;
end
%%  =================  FUNCIONS DE L'OPTIMITZADOR  ========================
%% OPTIMITZADOR (OPTIMITZADOR)
function optimizadorPopup_Callback(hObject, eventdata, handles)
    a = get(hObject, 'Value');
    switch(a)
        case 1  %Algoritmes Gen�tcs
            optimitzador ='genetics';
        case 2  %Pattern Search
            optimitzador = 'pattern';
    end
    
    if strcmp(optimitzador,'pattern')
        set(optimValor,'String','Pattern Search');
        set(generacionsEdit,'String',iteracionsLimit);
        set(generacionsValor,'String',iteracionsLimit);
        generacionsOptimitzador = iteracionsLimit;
        
%         set(iteracionsEdit,'Enable','off');
%         set(iteracionsValor,'Enable','off');
%         set(generacionsEdit,'Enable','off');
%         set(generacionsValor,'Enable','off');
    else
        set(optimValor,'String','Alg. Gen�ticos');
        set(generacionsEdit,'String',generacions);
        set(generacionsValor,'String',generacions);
        generacionsOptimitzador = generacions;
%         set(iteracionsEdit,'Enable','on');
%         set(iteracionsValor,'Enable','on');
%         set(generacionsEdit,'Enable','on');
%         set(generacionsValor,'Enable','on');
    end
end
%% ITERACIONS (OPTIMITZADOR)
function iteracions_Callback(hObject, eventdata, handles)
    user_entry = str2double(get(hObject,'string'));
    if isnan(user_entry)
        errordlg('You must enter a numeric value','Bad Input','modal')
        uicontrol(hObject)
        return
    end
    iteracionsOptimitzador = user_entry;
end
%% GENERACIONS (OPTIMITZADOR)
function generacions_Callback(hObject, eventdata, handles)
    user_entry = str2double(get(hObject,'string'));
    if isnan(user_entry)
        errordlg('El valor introducido debe ser num�rico','Bad Input','modal')
        uicontrol(hObject)
        return
    end
    generacionsOptimitzador = user_entry;
end
%% ------------------------------------------------------------------------
%% LEDS
    function led_Callback(source,eventdata)
        value = get(source,'Value');
        if value
            set(source,'Value',0);
        else
            set(source,'Value',1);
        end
    end
%% BOTO OK
function okBut_Callback(source,eventdata)
    tag = get(source,'Tag');
    switch tag
        case 'Criteris'
            indList    = indListCriteris;
            indicadors = indicadorsCriteris;
            if isempty(indList)
                set(criterisLed,'Background',[1 0 0]);
            else
                if ~isempty(indList{1})
                    set(criterisLed,'Background',[0 1 0]);
                else
                    set(criterisLed,'Background',[1 0 0]);
                    
                    if (strcmp(get(criterisLed,'Tag'),'dataOk') && ...
                            strcmp(get(entradesSortidesLed,'Tag'),'dataOk') && ...
                            strcmp(get(plantaLed,'Tag'),'dataOk') && ...
                            strcmp(get(optimizadorLed,'Tag'),'dataOk') && ...
                            strcmp(get(dataLed,'Tag'),'dataOk'))
                        set(runLed, 'Background',[0 1 0]);
                        set(run,'Enable','on');
                    else
                        set(runLed, 'Background',[1 0 0]);
                        set(run,'Enable','off');
                    end
                    delete(c);
                end
            end
            aux = [];
            indx = unique(indList(:,2));
            for j=1:length(indx)
                n = length(find(str2num(cell2mat(indList(:,2)))==str2num(indx{j})));
                aux = [aux strcat(criteris{1,1}(str2num(indx{j})),' (',num2str(n),')')];
            end
            set(criteriosPopup,'String',aux);
            delete(c);
        case 'Indicadors'
            indListCriteris    = indListCriterisSeleccio;
            if (criteriVisualitzacio==1)
                set(textAgreg,'String',indListCriteris(:,1));
            else
                index = strcmp(indListCriteris(:,2),num2str(criteriVisualitzacio-1));
                set(textAgreg,'String',indListCriteris(find(index==1),1));
            end
            delete(ind);
        case 'Aigua'
            aiguaEntrada = aiguaE;
            aiguaSortida = aiguaS;
            if aiguaEntrada>0 && aiguaSortida>0
                set(entradesSortidesLed, 'Background',[0 1 0]);
            end
            if aiguaEntrada>0, set(entradaValor,'String',Cin{1,aiguaEntrada});end;
            if aiguaSortida>0, set(salidaValor,'String',Cout{1,aiguaSortida});end;
            %Poso els NaN's a zero
            numCinSeleccio  = numCin;
            numCoutSeleccio = numCout;
            numCinSeleccio(find(isnan(numCin(:,aiguaEntrada))),aiguaEntrada) = 0;
            numCinSeleccio(find(numCin(:,aiguaEntrada)<numCout(:,aiguaSortida)),aiguaEntrada) = 0;
            componentsAOptimitzar = find(numCinSeleccio(:,aiguaEntrada));
            aiguaE = 0;
            aiguaS = 0;
            delete(ea);
        case 'Planta'
            cabalMin         = cabalMinPlanta;
            cabalMax         = cabalMaxPlanta;
            superficie       = superficiePlanta;
            tempsImplantacio = tempsImplantacioPlanta;
            numEtapes        = numEtapesPlanta;
            %if(cabalMax >= cabalMin && superficie>0 && tempsImplantacio>0)
            if(cabalMax >0 && superficie>0 && tempsImplantacio>0)
                set(plantaLed, 'Background',[0 1 0]);
            else
                set(plantaLed, 'Background',[1 0 0]);
            end
            set(cabMinValor,'String',cabalMin);
            set(cabMaxValor,'String',cabalMax);
            set(supValor,'String',superficie);
            set(tempsImpValor,'String',tempsImplantacio);
            close(p);
        case 'Optimitzador'
            iteracions  = iteracionsOptimitzador;
            
            switch(optimitzador)
                case 'genetics'
                    generacions = generacionsOptimitzador;
                case 'pattern'
                    iteracionsLimit = generacionsOptimitzador;
            end
            set(iteracionsValor,'String',iteracions);
            set(generacionsValor,'String',generacionsOptimitzador);
            close(o);
        case 'Resum'
            close();
            costos      = [];
            pesos       = [];
            %Concentradions d'entrada
            concEntradaMax = numData(5:end,5:3:4+3*length(contaminants{1}))';
            concEntradaMin = numData(5:end,6:3:5+3*length(contaminants{1}))';
            %Grau de reducci�
            reduccions = numData(5:end,(7:3:6+3*length(contaminants{1})))';
            reduccions(find(isnan(reduccions))) = 1;
            %Par�metres per la Planta (cabal min, max, duperf�cie i temps implantacio)
            dadesPlanta = [cabalMax, cabalMin, superficie, tempsImplantacio];
            a = tractamentsCandidats;
            %Criteris
            [costosMin, costosMax, dummy, dummy, pesos] = CostosPesos(a, false);
            
            disp(['Entrades     ' 'Sortides']);
            disp([numCinSeleccio(:,aiguaEntrada) numCoutSeleccio(:,aiguaSortida)]);
            disp('--- OPTIMITZACI� ---');
            disp(contaminants{1}(componentsAOptimitzar));
            disp(['Entrades     ' 'Sortides']);
            disp([numCinSeleccio(componentsAOptimitzar,aiguaEntrada) numCoutSeleccio(componentsAOptimitzar,aiguaSortida)]);
            disp('Components candidats');
            disp(rawData(a+5,1));
            
            switch(optimitzador)
                case 'genetics'
                    generacionsAux = generacions;
                case 'pattern'
                    generacionsAux = iteracionsLimit;
            end
            [solucio, noAssolits] = Init(numCinSeleccio(componentsAOptimitzar,aiguaEntrada),...
                                         numCoutSeleccio(componentsAOptimitzar,aiguaSortida),...
                                         concEntradaMax(componentsAOptimitzar,a),...
                                         concEntradaMin(componentsAOptimitzar,a),...
                                         reduccions(componentsAOptimitzar,a),...
                                         costosMin', costosMax', pesos, iteracions,...
                                         paramsPlanta(a,3), dadesPlanta(3),...
                                         generacionsAux,numEtapes,optimitzador);

            if ~isempty(solucio)
                if ~isempty(noAssolits)
                    h = errordlg(['Los siguientes par�metros no se han podido reducir: ' contaminants{1}(componentsAOptimitzar(noAssolits(end,:)))'],'Optimizaci�n Fallida');
                end
                
                [mostraSolucio,final] = calculaSolucions(solucio,size(solucio,1));
                solucionsDisponibles = {};
                for j=1:size(solucio,1)
                    solucionsDisponibles = [solucionsDisponibles {['Solucion ' num2str(j)]}];
                end
                
                set(exportLed,'Background',[0 1 0]);
                set(exportBut,'Enable','on');
            else
                mostraSolucio = 'No hay soluci�n factible con los datos seleccionados';
                final = ['-' '-' '-' '-'];
                solucionsDisponibles = {'-'};
                set(exportLed,'Background',[1 0 0]);
                set(exportBut,'Enable','off');
            end
            out          = dialog('WindowStyle', 'normal',...
                                   'Name','Resultados','Position',[15 70 400 600],...
                                   'Color',colfig);
            posOut       = get(out,'Position');
            
            resultatsPop = uicontrol('Style','popupmenu','String',solucionsDisponibles,...
                'Position',[10,550,posOut(3)-20,20],'Callback',{@resultatsPopup_Callback});
        
            textResultat  = uicontrol('Parent',out,'Style','listbox',...
                                   'String',mostraSolucio,'SelectionHighlight','off',...
                                   'Position',[10 200 posOut(3)-20 posOut(4)-20-260]);
            costImplantacioLabel      = uicontrol('Parent',out,'Style','text',...
                                   'String','Coste de implantaci�n                                     �/UF',...
                                   'FontSize',12,'ForegroundColor','white','BackgroundColor',colfig,...
                                   'HorizontalAlignment','Left','Position',[15 170 370 17]);
            costOperacioLabel      = uicontrol('Parent',out,'Style','text',...
                                   'String','Coste de operaci�n                                          �/UF/a�o',...
                                   'FontSize',12,'ForegroundColor','white','BackgroundColor',colfig,...
                                   'HorizontalAlignment','Left','Position',[15 140 370 17]);
            impacteAmbientalLabel  = uicontrol('Parent',out,'Style','text',...
                                   'String','Impacto ambiental                                             uds',...
                                   'FontSize',12,'ForegroundColor','white','BackgroundColor',colfig,...
                                   'HorizontalAlignment','Left','Position',[15 110 370 17]);
            valoritzacioLabel      = uicontrol('Parent',out,'Style','text',...
                                   'String','Valorizaci�n de subproductos                         %',...
                                   'FontSize',12,'ForegroundColor','white','BackgroundColor',colfig,...
                                   'HorizontalAlignment','Left','Position',[15 80 370 17]);
                               
                               
            if (iscell(final))                 
                a1 = sprintf('%0.1f',str2num(final{1}));
                a2 = sprintf('%0.1f',str2num(final{2}));
                a3 = sprintf('%0.1f',str2num(final{3}));
                a4 = sprintf('%0.1f',str2num(final{4}));
            else
                a1 = '-';
                a2 = '-';
                a3 = '-';
                a4 = '-';
            end
            
            costImplantacioValor      = uicontrol('Parent',out,'Style','text',...
                                   'String',a1,'Position',[230 170 80 17],... %final'
                                   'FontSize',12,'ForegroundColor','white','BackgroundColor',colfig,...
                                   'HorizontalAlignment','Right');
            costOperacioValor      = uicontrol('Parent',out,'Style','text',...
                                   'String',a2,'Position',[230 140 80 17],...
                                   'FontSize',12,'ForegroundColor','white','BackgroundColor',colfig,...
                                   'HorizontalAlignment','Right');
           impacteAmbientalValor  = uicontrol('Parent',out,'Style','text',...
                                   'String',a3,'Position',[230 110 80 17],...
                                   'FontSize',12,'ForegroundColor','white','BackgroundColor',colfig,...
                                   'HorizontalAlignment','Right');
                               
            valoritzacioValor      = uicontrol('Parent',out,'Style','text',...
                                   'String',a4,'Position',[230 80 80 17],...
                                   'FontSize',12,'ForegroundColor','white','BackgroundColor',colfig,...
                                   'HorizontalAlignment','Right');

            okResultsBut           = uicontrol('Parent',out,'Style', 'pushbutton', 'String', 'Aceptar',...
                                   'Position', [posOut(3)-110 10 100 30],...
                                   'Tag','Resultats','Callback', {@okBut_Callback});
            
            movegui(out,'center')
        otherwise
            close();
    end
    if(~strcmp(tag,'Indicadors'))
        if (strcmp(get(criterisLed,'Tag'),'dataOk') && ...
                strcmp(get(entradesSortidesLed,'Tag'),'dataOk') && ...
                strcmp(get(plantaLed,'Tag'),'dataOk') && ...
                strcmp(get(optimizadorLed,'Tag'),'dataOk') && ...
                strcmp(get(dataLed,'Tag'),'dataOk'))
            set(runLed, 'Background',[0 1 0]);
            set(run,'Enable','on');
        else
            set(runLed, 'Background',[1 0 0]);
            set(run,'Enable','off');
        end
    end
end
%% BOTO CANCEL
function cancelBut_Callback(source,eventdata)
    close(gcbf);
end
%% Primera seleccio de tractaments
function [out] = tractamentsCandidats
    dadesPlanta = [cabalMax, cabalMin, superficie, tempsImplantacio];
    paramsPlanta = numData(5:end,1:4);
    [cabalsMaxs, dummy] = find(paramsPlanta(:,1)>=dadesPlanta(1)); %Cabal m�xim
    [cabalsMins, dummy] = find(paramsPlanta(:,2)<=dadesPlanta(2)); %Cabal m�nim
    [tempsImpl,  dummy] = find(paramsPlanta(:,4)<=dadesPlanta(4)); %Temps implantaci�
    
    %Comprobaci� de la possibilitat de tractar cada par�metre
    offsetReduccions = 23;
%     reduccionsTotals = cumprod([numCin(:,aiguaEntrada)';numData(5:end,offsetReduccions:offsetReduccions+17)],1);
    reduccionsTotals = cumprod([numCinSeleccio(:,aiguaEntrada)';numData(5:end,7:3:6+3*length(contaminants{1}))],1);
    noTractatsTOTS   = find(numCoutSeleccio(:,aiguaSortida)' < reduccionsTotals(end,:));
    [noTractats, aux]= ismember(noTractatsTOTS,componentsAOptimitzar);
    if noTractats
        f = warndlg({'Atenci�n: Los siguientes par�metros no ser�n considerados por no tener tratamientos disponibles:',' ',contaminants{1}{componentsAOptimitzar(aux)}}, 'Filtro Par�metros');
        componentsAOptimitzar(aux) = [];
    end
    
    a = intersect(tempsImpl, intersect(cabalsMins,cabalsMaxs));
    out = a;
end
%% Criteris
function [costosImp, costosExp, ambient, product, pesos] = CostosPesos(tractaments,noNormalitzat)
    %Criteris
    costosImp     = [];
    costosExp     = [];
    ambient       = [];
    product       = [];
    pesos         = [];
    
    %C�lcul dels valors finals dels criteris sel�leccionats
    if noNormalitzat
        for k=1:length(criteris{1})
            llistaIndicadors = indList(strcmp(indList(:,2),num2str(k)),1);
            if ~isempty(strfind(criteris{1}{k},'Cost')) && ~isempty(strfind(criteris{1}{k},'Impl'))
                costosInt = [];
                if ~isempty(llistaIndicadors)
                    for t=1:length(llistaIndicadors)
                        ind    = find(strcmp(rawData(3,:),llistaIndicadors(t)))-1;
                        if(isempty(costosInt))
                            costosInt = numData(4+tractaments,ind);
                        else
                            costosInt = costosInt + numData(4+tractaments,ind);
                        end
                    end
                    costosImp = [costosImp costosInt];
                end
            elseif ~isempty(strfind(criteris{1}{k},'Cost')) && ~isempty(strfind(criteris{1}{k},'Expl'))
                costosInt = [];
                if ~isempty(llistaIndicadors)
                    for t=1:length(llistaIndicadors)
                        ind    = find(strcmp(rawData(3,:),llistaIndicadors(t)))-1;
                        if(isempty(costosInt))
                            costosInt = numData(4+tractaments,ind);
                        else
                            costosInt = costosInt + numData(4+tractaments,ind);
                        end
                    end
                    costosExp = [costosExp costosInt];
                end
            elseif ~isempty(strfind(criteris{1}{k},'Impact'))
                    if ~isempty(llistaIndicadors)
                        for t=1:length(llistaIndicadors)
                            ind        = find(strcmp(rawData(3,:),llistaIndicadors(t)))-1;
                            aux        = numData(4+tractaments,ind);
                            valorMaxim = max(aux);
                            if(isempty(ambient))
                                if (valorMaxim)
                                    ambient = aux./valorMaxim;
                                end
                            else
                                %ambient = ambient + numData(4+tractaments,ind);
                                if (valorMaxim)
                                    ambient = ambient + aux./valorMaxim;
                                end
                            end
                        end
                    end
            elseif ~isempty(strfind(criteris{1}{k},'Prod'))
                    if ~isempty(llistaIndicadors)
                        for t=1:length(llistaIndicadors)
                            ind    = find(strcmp(rawData(3,:),llistaIndicadors(t)))-1;
                            if(isempty(product))
                                product = numData(4+tractaments,ind);
                            else
                                product = product + numData(4+tractaments,ind);
                            end
                        end
                    end
            end
        end
        return
    end
    
    %C�lcul dels costos econ�mics normalitzats per a la funci� de cost
    for i=1:length(criteris{1})
        %Busco els indicadors corresponents als criteris 'i'
        llistaIndicadors = indList(strcmp(indList(:,2),num2str(i)),1);
        if ~isempty(strfind(criteris{1}{i},'Cost'))
            %------------ CRITERIS ECON�MICS --------------
                %Agafo les dades corresponents als indicadors
                %econ�mics i les sumo.
                costosInt = [];
                if ~isempty(llistaIndicadors)
                    for j=1:length(llistaIndicadors)
                        ind    = find(strcmp(rawData(3,:),llistaIndicadors(j)))-1;
                        if(isempty(costosInt))
                            costosInt = numData(4+tractaments,ind);
                        else
                            costosInt = costosInt + numData(4+tractaments,ind);
                        end
                    end
                    costosImp    = [costosImp costosInt/max(costosInt)];
                    pesos     = [pesos rawData{2,find(strcmp(rawData(1,:),criteris{1}(1)))}];
                end
        %elseif (~isempty(strfind(criteris{1}{i},'Impact')) || ~isempty(strfind(criteris{1}{i},'Prod')))
        elseif (~isempty(strfind(criteris{1}{i},'Impact')))
            pesosAmb      = 0;
            impct         = [];
            impacteAmbInt = [];
            if ~isempty(llistaIndicadors)
                for j=1:length(llistaIndicadors)
                    ind   = find(strcmp(rawData(3,:),llistaIndicadors(j)))-1;
                    if max(numData(4+tractaments,ind))
                        impct = numData(3,ind) * (numData(4+tractaments,ind)./max(numData(4+tractaments,ind)));
                    else
                        impct = numData(3,ind) * numData(4+tractaments,ind);
                    end
                    if(isempty(impacteAmbInt))
                        impacteAmbInt = impct;
                    else
                        impacteAmbInt = impacteAmbInt + impct;
                    end
                    if ~isempty(find(impct))
                        pesosAmb = pesosAmb + numData(3,ind);
                    end
                end
                costosImp = [costosImp impacteAmbInt / pesosAmb]; %??
                pesos  = [pesos rawData{2,find(strcmp(rawData(1,:),criteris{1}(i)))}];
            end
        elseif (~isempty(strfind(criteris{1}{i},'Prod')))
            pesosAmb      = 0;
            impct         = [];
            impacteAmbInt = [];
            if ~isempty(llistaIndicadors)
                for j=1:length(llistaIndicadors)
                    ind   = find(strcmp(rawData(3,:),llistaIndicadors(j)))-1;
                    if max(numData(4+tractaments,ind))
                        %impct = numData(3,ind) * (numData(4+tractaments,ind)./max(numData(4+tractaments,ind)));
                        impct = numData(3,ind) * (numData(4+tractaments,ind)./100); %Normalitzo respecte al 100%
                    else
                        impct = numData(3,ind) * numData(4+tractaments,ind);
                    end
                    if(isempty(impacteAmbInt))
                        impacteAmbInt = impct;
                    else
                        impacteAmbInt = impacteAmbInt + impct;
                    end
                    if ~isempty(find(impct))
                        pesosAmb = pesosAmb + numData(3,ind);
                    end
                end
                costosExp = [costosExp impacteAmbInt / pesosAmb]; %??
                pesos  = [pesos rawData{2,find(strcmp(rawData(1,:),criteris{1}(i)))}];
            end
        end
    end
    %--------
    
%     %C�lcul dels costos econ�mics normalitzats per a la funci� de cost
%     for i=1:length(criteris{1})
%         %Busco els indicadors corresponents als criteris 'i'
%         llistaIndicadors = indList(strcmp(indList(:,2),num2str(i)),1);
%         switch criteris{1}{i}%(i)
%             case criteris{1}{1}
%                 %------------ CRITERIS ECON�MICS --------------
%                 %Agafo les dades corresponents als indicadors
%                 %econ�mics i les sumo.
%                 if ~isempty(llistaIndicadors)
%                     for j=1:length(llistaIndicadors)
%                         ind    = find(strcmp(rawData(3,:),llistaIndicadors(j)))-1;
%                         if(isempty(costosInt))
%                             costosInt = numData(4+tractaments,ind);
%                         else
%                             costosInt = costosInt + numData(4+tractaments,ind);
%                         end
%                     end
%                     costos    = [costos costosInt/max(costosInt)];
%                     pesos     = [pesos rawData{2,find(strcmp(rawData(1,:),criteris{1}(1)))}];
%                 end
%             otherwise
%                 %------- IMPACTE AMBIENTAL --------------
%                 %Agafo les dades corresponents als indicadors
%                 %d'Impacte Ambiental, les normalitzo respecte al tractament m�s alt i les sumo.
%                 pesosAmb      = 0;
%                 impct         = [];
%                 impacteAmbInt = [];
%                 if ~isempty(llistaIndicadors)
%                     for j=1:length(llistaIndicadors)
%                         ind   = find(strcmp(rawData(3,:),llistaIndicadors(j)))-1;
%                         if max(numData(4+tractaments,ind))
%                             impct = numData(3,ind) * (numData(4+tractaments,ind)./max(numData(4+tractaments,ind)));
%                         else
%                             impct = numData(3,ind) * numData(4+tractaments,ind);
%                         end
%                         if(isempty(impacteAmbInt))
%                             impacteAmbInt = impct;
%                         else
%                             impacteAmbInt = impacteAmbInt + impct;
%                         end
%                         if ~isempty(find(impct))
%                             pesosAmb = pesosAmb + numData(3,ind);
%                         end
%                     end
%                     costos = [costos impacteAmbInt / pesosAmb]; %??
%                     pesos  = [pesos rawData{2,find(strcmp(rawData(1,:),criteris{1}(i)))}];
%                 end
%         end
%     end
end
%% Solucions
function [mostraSolucio,final] = calculaSolucions(solucions,solucio)
        
    final         = {};
    [ro, co]      = find(solucions(solucio,:));
    a             = tractamentsCandidats;
    aux           = rawData(a+5,1);
    mostraSolucio = aux(solucions(solucio,co));
    [auxCostImp, auxCostExp, auxAmbient, auxProd, auxPesos] = CostosPesos(a,true);
    
    %Resultat CostImplementacio
    if ~isempty(auxCostImp)
        aux = cumsum(auxCostImp((solucions(solucio,co)),1));
        costFinal = num2str(aux(end));
    else
        costFinal = '-';
    end
    final = [final costFinal];
    
    %Resultat CostExplotacio
    if ~isempty(auxCostExp)
        aux = cumsum(auxCostExp((solucions(solucio,co)),1));
        costFinal = num2str(aux(end));
    else
        costFinal = '-';
    end
    final = [final costFinal];
    
    %Resultat Ambient
    if ~isempty(auxAmbient)
        aux = cumsum(auxAmbient(solucions(solucio,co)));
        ambientFinal = num2str(aux(end));
    else
        ambientFinal = '-';
    end
    final = [final ambientFinal];
    
    %Resultat Productivitat
    if ~isempty(auxProd)
        aux = cumsum(auxProd(solucions(solucio,co)));
        prodFinal = num2str(aux(end));
    else
        prodFinal = '-';
    end
    final = [final prodFinal];
end

function resultatsPopup_Callback(hObject, eventdata, handles)
    a = get(hObject, 'Value');
    if ~(strcmp(get(hObject,'string'),'-'))
        [mostraSolucio,final] = calculaSolucions(solucio,size(solucio,1)-a+1);
        
        set(textResultat,'String',mostraSolucio);
        set(costImplantacioValor,'String',final(1));
        set(costOperacioValor,'String',final(2));
        set(impacteAmbientalValor,'String',final(3));
        set(valoritzacioValor,'String',final(4));
    end
end

%% Iguala dimensions E/S
    function [out] = dimensions(in,dimFiles,dimColumnes)
        
        auxIn = in;
        %files
        if size(auxIn,1)<dimFiles
            auxIn = [auxIn;zeros(dimFiles-size(auxIn,1),size(auxIn,2))];
        end
        %columnes
        if size(auxIn,2)<dimColumnes
            auxIn = [auxIn zeros(size(auxIn,1),dimColumnes-size(auxIn,2))];
        end
        out = auxIn;
    end
end